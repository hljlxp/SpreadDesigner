﻿using FarPoint.Win.Spread;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SpreadDesigner
{
  public class TabStripDlg : System.Windows.Forms.Form
{
    System.Resources.ResourceSet rm = null;
    private FarPoint.Win.Spread.FpSpread theControl;
	
	internal FarPoint.Win.Spread.DrawingSpace.FontPicker activeFontPicker;

	internal FarPoint.Win.Spread.DrawingSpace.FontPicker defaultFontPicker;
	#region " Windows Form Designer generated code "
	
	protected override void OnShown(EventArgs e)
	{
		this.TabStrip_GBPreview.Size = new System.Drawing.Size(TabStrip_GBPreview.Size.Width, 48);
		this.FpSpread1.Size = new System.Drawing.Size(FpSpread1.Size.Width, 20);
		base.OnShown(e);
	}
	
	public TabStripDlg() : base()
	{
    if (FarPoint.Win.Spread.Design.common.rm != null)
    {
      rm = FarPoint.Win.Spread.Design.common.rm;
    }
    else
    {
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      rm = FarPoint.Win.Spread.Design.common.rm;
    }
		Load += TabStripDlg_Load;

		//This call is required by the Windows Form Designer.
		InitializeComponent();

	}

	//24889 Tan 2009/03/12 change type of owner from FPSpread to SpreadWrapper
  public TabStripDlg(FarPoint.Win.Spread.FpSpread owner)
    : base()
	{
    if (FarPoint.Win.Spread.Design.common.rm != null)
    {
      rm = FarPoint.Win.Spread.Design.common.rm;
    }
    else
    {
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      rm = FarPoint.Win.Spread.Design.common.rm;
    }
		Load += TabStripDlg_Load;

		//This call is required by the Windows Form Designer.
		InitializeComponent();

		theControl = owner;

		InitValues();
	}

	//Form overrides dispose to clean up the component list.
	protected override void Dispose(bool disposing)
	{
		if (disposing) {
			if ((components != null)) {
				components.Dispose();
			}
		}
		base.Dispose(disposing);
	}

	//Required by the Windows Form Designer

	private System.ComponentModel.IContainer components;
	//NOTE: The following procedure is required by the Windows Form Designer
	//It can be modified using the Windows Form Designer.  
	//Do not modify it using the code editor.
	private System.Windows.Forms.Button withEventsField_TabStripApplyBtn;
	internal System.Windows.Forms.Button TabStripApplyBtn {
		get { return withEventsField_TabStripApplyBtn; }
		set {
			if (withEventsField_TabStripApplyBtn != null) {
				withEventsField_TabStripApplyBtn.Click -= TabStripApplyBtn_Click;
			}
			withEventsField_TabStripApplyBtn = value;
			if (withEventsField_TabStripApplyBtn != null) {
				withEventsField_TabStripApplyBtn.Click += TabStripApplyBtn_Click;
			}
		}
	}
	internal System.Windows.Forms.Button TabStripCancelBtn;
	private System.Windows.Forms.Button withEventsField_TabStripOKBtn;
	internal System.Windows.Forms.Button TabStripOKBtn {
		get { return withEventsField_TabStripOKBtn; }
		set {
			if (withEventsField_TabStripOKBtn != null) {
				withEventsField_TabStripOKBtn.Click -= TabStripOKBtn_Click;
			}
			withEventsField_TabStripOKBtn = value;
			if (withEventsField_TabStripOKBtn != null) {
				withEventsField_TabStripOKBtn.Click += TabStripOKBtn_Click;
			}
		}
	}
	internal System.Windows.Forms.TabControl TabStrip_TabControl;
	internal System.Windows.Forms.TabPage TabStrip_TabsGeneral;
	internal System.Windows.Forms.TabPage TabStrip_TabsDefault;
	internal System.Windows.Forms.TabPage TabStrip_TabsActive;
	internal System.Windows.Forms.GroupBox TabStrip_GBPreview;
	internal FarPoint.Win.Spread.FpSpread FpSpread1;
	internal FarPoint.Win.Spread.SheetView FpSpread1_Sheet1;
	internal System.Windows.Forms.Label TabStrip_TabStripPlacementLabel;
	private System.Windows.Forms.ComboBox withEventsField_TabStrip_TabStripPlacement;
	internal System.Windows.Forms.ComboBox TabStrip_TabStripPlacement {
		get { return withEventsField_TabStrip_TabStripPlacement; }
		set {
			if (withEventsField_TabStrip_TabStripPlacement != null) {
				withEventsField_TabStrip_TabStripPlacement.SelectedIndexChanged -= TabStrip_TabStripPlacement_SelectedIndexChanged;
			}
			withEventsField_TabStrip_TabStripPlacement = value;
			if (withEventsField_TabStrip_TabStripPlacement != null) {
				withEventsField_TabStrip_TabStripPlacement.SelectedIndexChanged += TabStrip_TabStripPlacement_SelectedIndexChanged;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_TabStripPolicyLabel;
	private System.Windows.Forms.ComboBox withEventsField_TabStrip_TabStripPolicy;
	internal System.Windows.Forms.ComboBox TabStrip_TabStripPolicy {
		get { return withEventsField_TabStrip_TabStripPolicy; }
		set {
			if (withEventsField_TabStrip_TabStripPolicy != null) {
				withEventsField_TabStrip_TabStripPolicy.SelectedIndexChanged -= TabStrip_TabStripPolicy_SelectedIndexChanged;
			}
			withEventsField_TabStrip_TabStripPolicy = value;
			if (withEventsField_TabStrip_TabStripPolicy != null) {
				withEventsField_TabStrip_TabStripPolicy.SelectedIndexChanged += TabStrip_TabStripPolicy_SelectedIndexChanged;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_TabStripButtonPolicyLabel;
	private System.Windows.Forms.ComboBox withEventsField_TabStrip_TabStripButtonPolicy;
	internal System.Windows.Forms.ComboBox TabStrip_TabStripButtonPolicy {
		get { return withEventsField_TabStrip_TabStripButtonPolicy; }
		set {
			if (withEventsField_TabStrip_TabStripButtonPolicy != null) {
				withEventsField_TabStrip_TabStripButtonPolicy.SelectedIndexChanged -= TabStrip_TabStripButtonPolicy_SelectedIndexChanged;
			}
			withEventsField_TabStrip_TabStripButtonPolicy = value;
			if (withEventsField_TabStrip_TabStripButtonPolicy != null) {
				withEventsField_TabStrip_TabStripButtonPolicy.SelectedIndexChanged += TabStrip_TabStripButtonPolicy_SelectedIndexChanged;
			}
		}
	}
	private System.Windows.Forms.CheckBox withEventsField_TabStrip_Editable;
	internal System.Windows.Forms.CheckBox TabStrip_Editable {
		get { return withEventsField_TabStrip_Editable; }
		set {
			if (withEventsField_TabStrip_Editable != null) {
				withEventsField_TabStrip_Editable.CheckedChanged -= TabStrip_Editable_CheckedChanged;
			}
			withEventsField_TabStrip_Editable = value;
			if (withEventsField_TabStrip_Editable != null) {
				withEventsField_TabStrip_Editable.CheckedChanged += TabStrip_Editable_CheckedChanged;
			}
		}
	}
	private System.Windows.Forms.Button withEventsField_TabStrip_BackColorSelect;
	internal System.Windows.Forms.Button TabStrip_BackColorSelect {
		get { return withEventsField_TabStrip_BackColorSelect; }
		set {
			if (withEventsField_TabStrip_BackColorSelect != null) {
				withEventsField_TabStrip_BackColorSelect.Click -= ColorSelect_Click;
			}
			withEventsField_TabStrip_BackColorSelect = value;
			if (withEventsField_TabStrip_BackColorSelect != null) {
				withEventsField_TabStrip_BackColorSelect.Click += ColorSelect_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_BackColor;
	internal System.Windows.Forms.Label TabStrip_BackColorLabel;
	internal System.Windows.Forms.Label TabStrip_RatioLabel;
	private System.Windows.Forms.NumericUpDown withEventsField_TabStrip_Ratio;
	internal System.Windows.Forms.NumericUpDown TabStrip_Ratio {
		get { return withEventsField_TabStrip_Ratio; }
		set {
			if (withEventsField_TabStrip_Ratio != null) {
				withEventsField_TabStrip_Ratio.TextChanged -= TabStrip_Ratio_TextChanged;
			}
			withEventsField_TabStrip_Ratio = value;
			if (withEventsField_TabStrip_Ratio != null) {
				withEventsField_TabStrip_Ratio.TextChanged += TabStrip_Ratio_TextChanged;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_RatioPercentLabel;
	private System.Windows.Forms.Button withEventsField_TabStrip_ActiveBackColorSelect;
	internal System.Windows.Forms.Button TabStrip_ActiveBackColorSelect {
		get { return withEventsField_TabStrip_ActiveBackColorSelect; }
		set {
			if (withEventsField_TabStrip_ActiveBackColorSelect != null) {
				withEventsField_TabStrip_ActiveBackColorSelect.Click -= ColorSelect_Click;
			}
			withEventsField_TabStrip_ActiveBackColorSelect = value;
			if (withEventsField_TabStrip_ActiveBackColorSelect != null) {
				withEventsField_TabStrip_ActiveBackColorSelect.Click += ColorSelect_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_ActiveBackColor;
	internal System.Windows.Forms.Label TabStrip_ActiveBackColorLabel;
	private System.Windows.Forms.Button withEventsField_TabStrip_ActiveForeColorSelect;
	internal System.Windows.Forms.Button TabStrip_ActiveForeColorSelect {
		get { return withEventsField_TabStrip_ActiveForeColorSelect; }
		set {
			if (withEventsField_TabStrip_ActiveForeColorSelect != null) {
				withEventsField_TabStrip_ActiveForeColorSelect.Click -= ColorSelect_Click;
			}
			withEventsField_TabStrip_ActiveForeColorSelect = value;
			if (withEventsField_TabStrip_ActiveForeColorSelect != null) {
				withEventsField_TabStrip_ActiveForeColorSelect.Click += ColorSelect_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_ActiveForeColor;
	internal System.Windows.Forms.Label TabStrip_ActiveForeColorLabel;
	internal System.Windows.Forms.Label TabStrip_ActiveTabSizeLabel;
	private System.Windows.Forms.TextBox withEventsField_TabStrip_ActiveTabSize;
	internal System.Windows.Forms.TextBox TabStrip_ActiveTabSize {
		get { return withEventsField_TabStrip_ActiveTabSize; }
		set {
			if (withEventsField_TabStrip_ActiveTabSize != null) {
				withEventsField_TabStrip_ActiveTabSize.TextChanged -= TabStrip_ActiveTabSize_TextChanged;
			}
			withEventsField_TabStrip_ActiveTabSize = value;
			if (withEventsField_TabStrip_ActiveTabSize != null) {
				withEventsField_TabStrip_ActiveTabSize.TextChanged += TabStrip_ActiveTabSize_TextChanged;
			}
		}
	}
	private System.Windows.Forms.Button withEventsField_TabStrip_DefaultBackColorSelect;
	internal System.Windows.Forms.Button TabStrip_DefaultBackColorSelect {
		get { return withEventsField_TabStrip_DefaultBackColorSelect; }
		set {
			if (withEventsField_TabStrip_DefaultBackColorSelect != null) {
				withEventsField_TabStrip_DefaultBackColorSelect.Click -= ColorSelect_Click;
			}
			withEventsField_TabStrip_DefaultBackColorSelect = value;
			if (withEventsField_TabStrip_DefaultBackColorSelect != null) {
				withEventsField_TabStrip_DefaultBackColorSelect.Click += ColorSelect_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_DefaultBackColor;
	internal System.Windows.Forms.Label TabStrip_DefaultBackColorLabel;
	private System.Windows.Forms.Button withEventsField_TabStrip_DefaultForeColorSelect;
	internal System.Windows.Forms.Button TabStrip_DefaultForeColorSelect {
		get { return withEventsField_TabStrip_DefaultForeColorSelect; }
		set {
			if (withEventsField_TabStrip_DefaultForeColorSelect != null) {
				withEventsField_TabStrip_DefaultForeColorSelect.Click -= ColorSelect_Click;
			}
			withEventsField_TabStrip_DefaultForeColorSelect = value;
			if (withEventsField_TabStrip_DefaultForeColorSelect != null) {
				withEventsField_TabStrip_DefaultForeColorSelect.Click += ColorSelect_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_DefaultForeColor;
	internal System.Windows.Forms.Label TabStrip_DefaultForeColorLabel;
	internal System.Windows.Forms.Label TabStrip_DefaultTabSizeLabel;
	private System.Windows.Forms.TextBox withEventsField_TabStrip_DefaultTabSize;
	internal System.Windows.Forms.TextBox TabStrip_DefaultTabSize {
		get { return withEventsField_TabStrip_DefaultTabSize; }
		set {
			if (withEventsField_TabStrip_DefaultTabSize != null) {
				withEventsField_TabStrip_DefaultTabSize.TextChanged -= TabStrip_DefaultTabSize_TextChanged;
			}
			withEventsField_TabStrip_DefaultTabSize = value;
			if (withEventsField_TabStrip_DefaultTabSize != null) {
				withEventsField_TabStrip_DefaultTabSize.TextChanged += TabStrip_DefaultTabSize_TextChanged;
			}
		}
	}
	internal System.Windows.Forms.GroupBox TabStrip_DefaultGBFont;
	internal System.Windows.Forms.GroupBox TabStrip_ActiveGBFont;
	private System.Windows.Forms.ContextMenu withEventsField_TabStrip_ContextMenu;
	internal System.Windows.Forms.ContextMenu TabStrip_ContextMenu {
		get { return withEventsField_TabStrip_ContextMenu; }
		set {
			if (withEventsField_TabStrip_ContextMenu != null) {
				withEventsField_TabStrip_ContextMenu.Popup -= TabStrip_ContextMenu_Popup;
			}
			withEventsField_TabStrip_ContextMenu = value;
			if (withEventsField_TabStrip_ContextMenu != null) {
				withEventsField_TabStrip_ContextMenu.Popup += TabStrip_ContextMenu_Popup;
			}
		}
	}
	private System.Windows.Forms.MenuItem withEventsField_TabStrip_ContextMenu_Reset;
	internal System.Windows.Forms.MenuItem TabStrip_ContextMenu_Reset {
		get { return withEventsField_TabStrip_ContextMenu_Reset; }
		set {
			if (withEventsField_TabStrip_ContextMenu_Reset != null) {
				withEventsField_TabStrip_ContextMenu_Reset.Click -= TabStrip_ContextMenu_Reset_Click;
			}
			withEventsField_TabStrip_ContextMenu_Reset = value;
			if (withEventsField_TabStrip_ContextMenu_Reset != null) {
				withEventsField_TabStrip_ContextMenu_Reset.Click += TabStrip_ContextMenu_Reset_Click;
			}
		}
	}
	internal System.Windows.Forms.Label TabStrip_PreviewInfoLabel;
	private System.Windows.Forms.Button withEventsField_TabStripHelpBtn;
	internal System.Windows.Forms.Button TabStripHelpBtn {
		get { return withEventsField_TabStripHelpBtn; }
		set {
			if (withEventsField_TabStripHelpBtn != null) {
				withEventsField_TabStripHelpBtn.Click -= TabStripHelpBtn_Click;
			}
			withEventsField_TabStripHelpBtn = value;
			if (withEventsField_TabStripHelpBtn != null) {
				withEventsField_TabStripHelpBtn.Click += TabStripHelpBtn_Click;
			}
		}

	}
	[System.Diagnostics.DebuggerStepThrough()]
	
	private void InitializeComponent()
	{
		FarPoint.Localization.LocalizationResourceManager resources = new FarPoint.Localization.LocalizationResourceManager(typeof(TabStripDlg));
		this.TabStripApplyBtn = new System.Windows.Forms.Button();
		this.TabStripCancelBtn = new System.Windows.Forms.Button();
		this.TabStripOKBtn = new System.Windows.Forms.Button();
		this.TabStrip_TabControl = new System.Windows.Forms.TabControl();
		this.TabStrip_TabsGeneral = new System.Windows.Forms.TabPage();
		this.TabStrip_RatioPercentLabel = new System.Windows.Forms.Label();
		this.TabStrip_Editable = new System.Windows.Forms.CheckBox();
		this.TabStrip_RatioLabel = new System.Windows.Forms.Label();
		this.TabStrip_Ratio = new System.Windows.Forms.NumericUpDown();
		this.TabStrip_BackColorSelect = new System.Windows.Forms.Button();
		this.TabStrip_BackColor = new System.Windows.Forms.Label();
		this.TabStrip_BackColorLabel = new System.Windows.Forms.Label();
		this.TabStrip_TabStripButtonPolicy = new System.Windows.Forms.ComboBox();
		this.TabStrip_TabStripButtonPolicyLabel = new System.Windows.Forms.Label();
		this.TabStrip_TabStripPolicy = new System.Windows.Forms.ComboBox();
		this.TabStrip_TabStripPolicyLabel = new System.Windows.Forms.Label();
		this.TabStrip_TabStripPlacement = new System.Windows.Forms.ComboBox();
		this.TabStrip_TabStripPlacementLabel = new System.Windows.Forms.Label();
		this.TabStrip_TabsDefault = new System.Windows.Forms.TabPage();
		this.TabStrip_DefaultGBFont = new System.Windows.Forms.GroupBox();
		this.TabStrip_DefaultTabSize = new System.Windows.Forms.TextBox();
		this.TabStrip_DefaultTabSizeLabel = new System.Windows.Forms.Label();
		this.TabStrip_DefaultForeColorSelect = new System.Windows.Forms.Button();
		this.TabStrip_DefaultForeColor = new System.Windows.Forms.Label();
		this.TabStrip_DefaultForeColorLabel = new System.Windows.Forms.Label();
		this.TabStrip_DefaultBackColorSelect = new System.Windows.Forms.Button();
		this.TabStrip_DefaultBackColor = new System.Windows.Forms.Label();
		this.TabStrip_DefaultBackColorLabel = new System.Windows.Forms.Label();
		this.TabStrip_TabsActive = new System.Windows.Forms.TabPage();
		this.TabStrip_ActiveGBFont = new System.Windows.Forms.GroupBox();
		this.TabStrip_ActiveTabSize = new System.Windows.Forms.TextBox();
		this.TabStrip_ActiveTabSizeLabel = new System.Windows.Forms.Label();
		this.TabStrip_ActiveForeColorSelect = new System.Windows.Forms.Button();
		this.TabStrip_ActiveForeColor = new System.Windows.Forms.Label();
		this.TabStrip_ActiveForeColorLabel = new System.Windows.Forms.Label();
		this.TabStrip_ActiveBackColorSelect = new System.Windows.Forms.Button();
		this.TabStrip_ActiveBackColor = new System.Windows.Forms.Label();
		this.TabStrip_ActiveBackColorLabel = new System.Windows.Forms.Label();
		this.TabStrip_GBPreview = new System.Windows.Forms.GroupBox();
		this.FpSpread1 = new FarPoint.Win.Spread.FpSpread();
		this.FpSpread1_Sheet1 = new FarPoint.Win.Spread.SheetView();
		this.TabStrip_PreviewInfoLabel = new System.Windows.Forms.Label();
		this.TabStrip_ContextMenu = new System.Windows.Forms.ContextMenu();
		this.TabStrip_ContextMenu_Reset = new System.Windows.Forms.MenuItem();
		this.TabStripHelpBtn = new System.Windows.Forms.Button();
		this.TabStrip_TabControl.SuspendLayout();
		this.TabStrip_TabsGeneral.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)this.TabStrip_Ratio).BeginInit();
		this.TabStrip_TabsDefault.SuspendLayout();
		this.TabStrip_TabsActive.SuspendLayout();
		this.TabStrip_GBPreview.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)this.FpSpread1).BeginInit();
		((System.ComponentModel.ISupportInitialize)this.FpSpread1_Sheet1).BeginInit();
		this.SuspendLayout();
		//
		//TabStripApplyBtn
		//
		this.TabStripApplyBtn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
		this.TabStripApplyBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStripApplyBtn.Location = new System.Drawing.Point(324, 389);
		this.TabStripApplyBtn.Name = "TabStripApplyBtn";
		this.TabStripApplyBtn.Size = new System.Drawing.Size(80, 24);
		this.TabStripApplyBtn.TabIndex = 17;
		this.TabStripApplyBtn.Text = "Apply";
		//
		//TabStripCancelBtn
		//
		this.TabStripCancelBtn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
		this.TabStripCancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
		this.TabStripCancelBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStripCancelBtn.Location = new System.Drawing.Point(238, 389);
		this.TabStripCancelBtn.Name = "TabStripCancelBtn";
		this.TabStripCancelBtn.Size = new System.Drawing.Size(80, 24);
		this.TabStripCancelBtn.TabIndex = 16;
		this.TabStripCancelBtn.Text = "Cancel";
		//
		//TabStripOKBtn
		//
		this.TabStripOKBtn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
		this.TabStripOKBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
		this.TabStripOKBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStripOKBtn.Location = new System.Drawing.Point(152, 389);
		this.TabStripOKBtn.Name = "TabStripOKBtn";
		this.TabStripOKBtn.Size = new System.Drawing.Size(80, 24);
		this.TabStripOKBtn.TabIndex = 15;
		this.TabStripOKBtn.Text = "OK";
		//
		//TabStrip_TabControl
		//
		this.TabStrip_TabControl.Controls.AddRange(new System.Windows.Forms.Control[] {
			this.TabStrip_TabsGeneral,
			this.TabStrip_TabsDefault,
			this.TabStrip_TabsActive
		});
		this.TabStrip_TabControl.Location = new System.Drawing.Point(8, 8);
		this.TabStrip_TabControl.Name = "TabStrip_TabControl";
		this.TabStrip_TabControl.SelectedIndex = 0;
		this.TabStrip_TabControl.Size = new System.Drawing.Size(480, 288);
		this.TabStrip_TabControl.TabIndex = 0;
		//
		//TabStrip_TabsGeneral
		//
		this.TabStrip_TabsGeneral.BackColor = System.Drawing.Color.WhiteSmoke;
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_RatioPercentLabel);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_Editable);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_RatioLabel);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_Ratio);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_BackColorSelect);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_BackColor);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_BackColorLabel);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripButtonPolicy);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripButtonPolicyLabel);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripPolicy);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripPolicyLabel);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripPlacement);
		this.TabStrip_TabsGeneral.Controls.Add(this.TabStrip_TabStripPlacementLabel);
		this.TabStrip_TabsGeneral.Location = new System.Drawing.Point(4, 22);
		this.TabStrip_TabsGeneral.Name = "TabStrip_TabsGeneral";
		this.TabStrip_TabsGeneral.Size = new System.Drawing.Size(472, 262);
		this.TabStrip_TabsGeneral.TabIndex = 0;
		this.TabStrip_TabsGeneral.Text = "General";
		//
		//TabStrip_RatioPercentLabel
		//
		this.TabStrip_RatioPercentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
		this.TabStrip_RatioPercentLabel.Location = new System.Drawing.Point(315, 177);
		this.TabStrip_RatioPercentLabel.Name = "TabStrip_RatioPercentLabel";
		this.TabStrip_RatioPercentLabel.Size = new System.Drawing.Size(32, 18);
		this.TabStrip_RatioPercentLabel.TabIndex = 13;
		this.TabStrip_RatioPercentLabel.Text = "%";
		//
		//TabStrip_Editable
		//
		this.TabStrip_Editable.Location = new System.Drawing.Point(21, 216);
		this.TabStrip_Editable.Name = "TabStrip_Editable";
		this.TabStrip_Editable.Size = new System.Drawing.Size(160, 24);
		this.TabStrip_Editable.TabIndex = 11;
		this.TabStrip_Editable.Text = "&Editable";
		//
		//TabStrip_RatioLabel
		//
		this.TabStrip_RatioLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_RatioLabel.Location = new System.Drawing.Point(19, 176);
		this.TabStrip_RatioLabel.Name = "TabStrip_RatioLabel";
		this.TabStrip_RatioLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_RatioLabel.TabIndex = 12;
		this.TabStrip_RatioLabel.Text = "TabStripRatio (as percentage):";
		//
		//TabStrip_Ratio
		//
		this.TabStrip_Ratio.Location = new System.Drawing.Point(213, 176);
		this.TabStrip_Ratio.Name = "TabStrip_Ratio";
		this.TabStrip_Ratio.Size = new System.Drawing.Size(99, 20);
		this.TabStrip_Ratio.TabIndex = 10;
		this.TabStrip_Ratio.Value = new decimal(new int[] {
			50,
			0,
			0,
			0
		});
		//
		//TabStrip_BackColorSelect
		//
		this.TabStrip_BackColorSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_BackColorSelect.Location = new System.Drawing.Point(312, 121);
		this.TabStrip_BackColorSelect.Name = "TabStrip_BackColorSelect";
		this.TabStrip_BackColorSelect.Size = new System.Drawing.Size(30, 30);
		this.TabStrip_BackColorSelect.TabIndex = 8;
		this.TabStrip_BackColorSelect.Text = "...";
		
		this.TabStrip_BackColorSelect.AutoSize = true;
		
		//
		//TabStrip_BackColor
		//
		this.TabStrip_BackColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
		this.TabStrip_BackColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_BackColor.Location = new System.Drawing.Point(213, 121);
		this.TabStrip_BackColor.Name = "TabStrip_BackColor";
		this.TabStrip_BackColor.Size = new System.Drawing.Size(96, 32);
		this.TabStrip_BackColor.TabIndex = 7;
		this.TabStrip_BackColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		//
		//TabStrip_BackColorLabel
		//
		this.TabStrip_BackColorLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_BackColorLabel.Location = new System.Drawing.Point(19, 129);
		this.TabStrip_BackColorLabel.Name = "TabStrip_BackColorLabel";
		this.TabStrip_BackColorLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_BackColorLabel.TabIndex = 6;
		this.TabStrip_BackColorLabel.Text = "Back&Color:";
		//
		//TabStrip_TabStripButtonPolicy
		//
		this.TabStrip_TabStripButtonPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
		this.TabStrip_TabStripButtonPolicy.Location = new System.Drawing.Point(211, 84);
		this.TabStrip_TabStripButtonPolicy.Name = "TabStrip_TabStripButtonPolicy";
		this.TabStrip_TabStripButtonPolicy.Size = new System.Drawing.Size(232, 21);
		this.TabStrip_TabStripButtonPolicy.TabIndex = 5;
		//
		//TabStrip_TabStripButtonPolicyLabel
		//
		this.TabStrip_TabStripButtonPolicyLabel.Location = new System.Drawing.Point(19, 84);
		this.TabStrip_TabStripButtonPolicyLabel.Name = "TabStrip_TabStripButtonPolicyLabel";
		this.TabStrip_TabStripButtonPolicyLabel.Size = new System.Drawing.Size(184, 16);
		this.TabStrip_TabStripButtonPolicyLabel.TabIndex = 4;
		this.TabStrip_TabStripButtonPolicyLabel.Text = "&ButtonPolicy:";
		//
		//TabStrip_TabStripPolicy
		//
		this.TabStrip_TabStripPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
		this.TabStrip_TabStripPolicy.Location = new System.Drawing.Point(211, 51);
		this.TabStrip_TabStripPolicy.Name = "TabStrip_TabStripPolicy";
		this.TabStrip_TabStripPolicy.Size = new System.Drawing.Size(232, 21);
		this.TabStrip_TabStripPolicy.TabIndex = 3;
		//
		//TabStrip_TabStripPolicyLabel
		//
		this.TabStrip_TabStripPolicyLabel.Location = new System.Drawing.Point(19, 52);
		this.TabStrip_TabStripPolicyLabel.Name = "TabStrip_TabStripPolicyLabel";
		this.TabStrip_TabStripPolicyLabel.Size = new System.Drawing.Size(184, 16);
		this.TabStrip_TabStripPolicyLabel.TabIndex = 2;
		this.TabStrip_TabStripPolicyLabel.Text = "TabStrip&Policy:";
		//
		//TabStrip_TabStripPlacement
		//
		this.TabStrip_TabStripPlacement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
		this.TabStrip_TabStripPlacement.Location = new System.Drawing.Point(211, 18);
		this.TabStrip_TabStripPlacement.Name = "TabStrip_TabStripPlacement";
		this.TabStrip_TabStripPlacement.Size = new System.Drawing.Size(232, 21);
		this.TabStrip_TabStripPlacement.TabIndex = 1;
		//
		//TabStrip_TabStripPlacementLabel
		//
		this.TabStrip_TabStripPlacementLabel.Location = new System.Drawing.Point(19, 20);
		this.TabStrip_TabStripPlacementLabel.Name = "TabStrip_TabStripPlacementLabel";
		this.TabStrip_TabStripPlacementLabel.Size = new System.Drawing.Size(184, 16);
		this.TabStrip_TabStripPlacementLabel.TabIndex = 0;
		this.TabStrip_TabStripPlacementLabel.Text = "Tab&StripPlacement:";
		//
		//TabStrip_TabsDefault
		//
		this.TabStrip_TabsDefault.BackColor = System.Drawing.Color.WhiteSmoke;
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultGBFont);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultTabSize);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultTabSizeLabel);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultForeColorSelect);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultForeColor);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultForeColorLabel);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultBackColorSelect);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultBackColor);
		this.TabStrip_TabsDefault.Controls.Add(this.TabStrip_DefaultBackColorLabel);
		this.TabStrip_TabsDefault.Location = new System.Drawing.Point(4, 22);
		this.TabStrip_TabsDefault.Name = "TabStrip_TabsDefault";
		this.TabStrip_TabsDefault.Size = new System.Drawing.Size(472, 262);
		this.TabStrip_TabsDefault.TabIndex = 1;
		this.TabStrip_TabsDefault.Text = "Default Tab";
		//
		//TabStrip_DefaultGBFont
		//
		this.TabStrip_DefaultGBFont.Location = new System.Drawing.Point(16, 128);
		this.TabStrip_DefaultGBFont.Name = "TabStrip_DefaultGBFont";
		this.TabStrip_DefaultGBFont.Size = new System.Drawing.Size(372, 108);
		this.TabStrip_DefaultGBFont.TabIndex = 8;
		this.TabStrip_DefaultGBFont.TabStop = false;
		this.TabStrip_DefaultGBFont.Text = "Font";
		//
		//TabStrip_DefaultTabSize
		//
		this.TabStrip_DefaultTabSize.Location = new System.Drawing.Point(213, 91);
		this.TabStrip_DefaultTabSize.Name = "TabStrip_DefaultTabSize";
		this.TabStrip_DefaultTabSize.Size = new System.Drawing.Size(96, 20);
		this.TabStrip_DefaultTabSize.TabIndex = 7;
		this.TabStrip_DefaultTabSize.Text = "";
		//
		//TabStrip_DefaultTabSizeLabel
		//
		this.TabStrip_DefaultTabSizeLabel.Location = new System.Drawing.Point(19, 93);
		this.TabStrip_DefaultTabSizeLabel.Name = "TabStrip_DefaultTabSizeLabel";
		this.TabStrip_DefaultTabSizeLabel.Size = new System.Drawing.Size(184, 16);
		this.TabStrip_DefaultTabSizeLabel.TabIndex = 6;
		this.TabStrip_DefaultTabSizeLabel.Text = "&Tab Size:";
		//
		//TabStrip_DefaultForeColorSelect
		//
		this.TabStrip_DefaultForeColorSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultForeColorSelect.Location = new System.Drawing.Point(312, 52);
		this.TabStrip_DefaultForeColorSelect.Name = "TabStrip_DefaultForeColorSelect";
		this.TabStrip_DefaultForeColorSelect.Size = new System.Drawing.Size(30, 30);
		this.TabStrip_DefaultForeColorSelect.TabIndex = 5;
		this.TabStrip_DefaultForeColorSelect.Text = "...";
		this.TabStrip_DefaultForeColorSelect.AutoSize = true;
		//
		//TabStrip_DefaultForeColor
		//
		this.TabStrip_DefaultForeColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
		this.TabStrip_DefaultForeColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultForeColor.Location = new System.Drawing.Point(213, 52);
		this.TabStrip_DefaultForeColor.Name = "TabStrip_DefaultForeColor";
		this.TabStrip_DefaultForeColor.Size = new System.Drawing.Size(96, 32);
		this.TabStrip_DefaultForeColor.TabIndex = 4;
		this.TabStrip_DefaultForeColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		//
		//TabStrip_DefaultForeColorLabel
		//
		this.TabStrip_DefaultForeColorLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultForeColorLabel.Location = new System.Drawing.Point(19, 60);
		this.TabStrip_DefaultForeColorLabel.Name = "TabStrip_DefaultForeColorLabel";
		this.TabStrip_DefaultForeColorLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_DefaultForeColorLabel.TabIndex = 3;
		this.TabStrip_DefaultForeColorLabel.Text = "&ForeColor:";
		//
		//TabStrip_DefaultBackColorSelect
		//
		this.TabStrip_DefaultBackColorSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultBackColorSelect.Location = new System.Drawing.Point(312, 18);
		this.TabStrip_DefaultBackColorSelect.Name = "TabStrip_DefaultBackColorSelect";
		this.TabStrip_DefaultBackColorSelect.Size = new System.Drawing.Size(30, 30);
		this.TabStrip_DefaultBackColorSelect.TabIndex = 2;
		this.TabStrip_DefaultBackColorSelect.Text = "...";
		this.TabStrip_DefaultBackColorSelect.AutoSize = true;
		//
		//TabStrip_DefaultBackColor
		//
		this.TabStrip_DefaultBackColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
		this.TabStrip_DefaultBackColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultBackColor.Location = new System.Drawing.Point(213, 18);
		this.TabStrip_DefaultBackColor.Name = "TabStrip_DefaultBackColor";
		this.TabStrip_DefaultBackColor.Size = new System.Drawing.Size(96, 32);
		this.TabStrip_DefaultBackColor.TabIndex = 1;
		this.TabStrip_DefaultBackColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		//
		//TabStrip_DefaultBackColorLabel
		//
		this.TabStrip_DefaultBackColorLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_DefaultBackColorLabel.Location = new System.Drawing.Point(19, 26);
		this.TabStrip_DefaultBackColorLabel.Name = "TabStrip_DefaultBackColorLabel";
		this.TabStrip_DefaultBackColorLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_DefaultBackColorLabel.TabIndex = 0;
		this.TabStrip_DefaultBackColorLabel.Text = "&BackColor:";
		//
		//TabStrip_TabsActive
		//
		this.TabStrip_TabsActive.BackColor = System.Drawing.Color.WhiteSmoke;
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveGBFont);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveTabSize);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveTabSizeLabel);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveForeColorSelect);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveForeColor);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveForeColorLabel);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveBackColorSelect);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveBackColor);
		this.TabStrip_TabsActive.Controls.Add(this.TabStrip_ActiveBackColorLabel);
		this.TabStrip_TabsActive.Location = new System.Drawing.Point(4, 22);
		this.TabStrip_TabsActive.Name = "TabStrip_TabsActive";
		this.TabStrip_TabsActive.Size = new System.Drawing.Size(472, 262);
		this.TabStrip_TabsActive.TabIndex = 2;
		this.TabStrip_TabsActive.Text = "Active Tab";
		//
		//TabStrip_ActiveGBFont
		//
		this.TabStrip_ActiveGBFont.Location = new System.Drawing.Point(16, 128);
		this.TabStrip_ActiveGBFont.Name = "TabStrip_ActiveGBFont";
		this.TabStrip_ActiveGBFont.Size = new System.Drawing.Size(372, 108);
		this.TabStrip_ActiveGBFont.TabIndex = 9;
		this.TabStrip_ActiveGBFont.TabStop = false;
		this.TabStrip_ActiveGBFont.Text = "Font";
		//
		//TabStrip_ActiveTabSize
		//
		this.TabStrip_ActiveTabSize.Location = new System.Drawing.Point(213, 91);
		this.TabStrip_ActiveTabSize.Name = "TabStrip_ActiveTabSize";
		this.TabStrip_ActiveTabSize.Size = new System.Drawing.Size(96, 20);
		this.TabStrip_ActiveTabSize.TabIndex = 7;
		this.TabStrip_ActiveTabSize.Text = "";
		//
		//TabStrip_ActiveTabSizeLabel
		//
		this.TabStrip_ActiveTabSizeLabel.Location = new System.Drawing.Point(19, 93);
		this.TabStrip_ActiveTabSizeLabel.Name = "TabStrip_ActiveTabSizeLabel";
		this.TabStrip_ActiveTabSizeLabel.Size = new System.Drawing.Size(184, 16);
		this.TabStrip_ActiveTabSizeLabel.TabIndex = 6;
		this.TabStrip_ActiveTabSizeLabel.Text = "&Tab Size:";
		//
		//TabStrip_ActiveForeColorSelect
		//
		this.TabStrip_ActiveForeColorSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveForeColorSelect.Location = new System.Drawing.Point(312, 52);
		this.TabStrip_ActiveForeColorSelect.Name = "TabStrip_ActiveForeColorSelect";
		this.TabStrip_ActiveForeColorSelect.Size = new System.Drawing.Size(30, 30);
		this.TabStrip_ActiveForeColorSelect.TabIndex = 5;
		this.TabStrip_ActiveForeColorSelect.Text = "...";
		this.TabStrip_ActiveForeColorSelect.AutoSize = true;
		//
		//TabStrip_ActiveForeColor
		//
		this.TabStrip_ActiveForeColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
		this.TabStrip_ActiveForeColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveForeColor.Location = new System.Drawing.Point(213, 52);
		this.TabStrip_ActiveForeColor.Name = "TabStrip_ActiveForeColor";
		this.TabStrip_ActiveForeColor.Size = new System.Drawing.Size(96, 32);
		this.TabStrip_ActiveForeColor.TabIndex = 4;
		this.TabStrip_ActiveForeColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		//
		//TabStrip_ActiveForeColorLabel
		//
		this.TabStrip_ActiveForeColorLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveForeColorLabel.Location = new System.Drawing.Point(19, 60);
		this.TabStrip_ActiveForeColorLabel.Name = "TabStrip_ActiveForeColorLabel";
		this.TabStrip_ActiveForeColorLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_ActiveForeColorLabel.TabIndex = 3;
		this.TabStrip_ActiveForeColorLabel.Text = "&ForeColor:";
		//
		//TabStrip_ActiveBackColorSelect
		//
		this.TabStrip_ActiveBackColorSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveBackColorSelect.Location = new System.Drawing.Point(312, 18);
		this.TabStrip_ActiveBackColorSelect.Name = "TabStrip_ActiveBackColorSelect";
		this.TabStrip_ActiveBackColorSelect.Size = new System.Drawing.Size(30, 30);
		this.TabStrip_ActiveBackColorSelect.TabIndex = 2;
		this.TabStrip_ActiveBackColorSelect.Text = "...";
		this.TabStrip_ActiveBackColorSelect.AutoSize = true;
		//
		//TabStrip_ActiveBackColor
		//
		this.TabStrip_ActiveBackColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
		this.TabStrip_ActiveBackColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveBackColor.Location = new System.Drawing.Point(213, 18);
		this.TabStrip_ActiveBackColor.Name = "TabStrip_ActiveBackColor";
		this.TabStrip_ActiveBackColor.Size = new System.Drawing.Size(96, 32);
		this.TabStrip_ActiveBackColor.TabIndex = 1;
		this.TabStrip_ActiveBackColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		//
		//TabStrip_ActiveBackColorLabel
		//
		this.TabStrip_ActiveBackColorLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_ActiveBackColorLabel.Location = new System.Drawing.Point(19, 26);
		this.TabStrip_ActiveBackColorLabel.Name = "TabStrip_ActiveBackColorLabel";
		this.TabStrip_ActiveBackColorLabel.Size = new System.Drawing.Size(184, 20);
		this.TabStrip_ActiveBackColorLabel.TabIndex = 0;
		this.TabStrip_ActiveBackColorLabel.Text = "&BackColor:";
		//
		//TabStrip_GBPreview
		//
		this.TabStrip_GBPreview.BackColor = System.Drawing.Color.WhiteSmoke;
		this.TabStrip_GBPreview.Controls.Add(this.FpSpread1);
		this.TabStrip_GBPreview.Location = new System.Drawing.Point(8, 308);
		this.TabStrip_GBPreview.Name = "TabStrip_GBPreview";
		this.TabStrip_GBPreview.Size = new System.Drawing.Size(480, 48);
		this.TabStrip_GBPreview.TabIndex = 14;
		this.TabStrip_GBPreview.TabStop = false;
		this.TabStrip_GBPreview.Text = "Preview";
		//
		//FpSpread1
		//
		this.FpSpread1.About = "3.0.1005.2002";
		this.FpSpread1.AccessibleDescription = "";
		this.FpSpread1.AutoClipboard = false;
		this.FpSpread1.ColumnSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never;
		this.FpSpread1.Location = new System.Drawing.Point(16, 18);
		this.FpSpread1.Name = "FpSpread1";
		this.FpSpread1.RowSplitBoxPolicy = FarPoint.Win.Spread.SplitBoxPolicy.Never;
		this.FpSpread1.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] { this.FpSpread1_Sheet1 });
		this.FpSpread1.Size = new System.Drawing.Size(448, 20);
		this.FpSpread1.TabIndex = 0;
		this.FpSpread1.TabStripPolicy = FarPoint.Win.Spread.TabStripPolicy.Always;
		this.FpSpread1.TabStripRatio = 1;
		this.FpSpread1.VerticalScrollBarPolicy = FarPoint.Win.Spread.ScrollBarPolicy.Never;
		//
		//FpSpread1_Sheet1
		//
		this.FpSpread1_Sheet1.Reset();
		this.FpSpread1_Sheet1.SheetName = "Sheet1";
		//Formulas and custom names must be loaded with R1C1 reference style
		this.FpSpread1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
		this.FpSpread1_Sheet1.ColumnHeader.Visible = false;
		this.FpSpread1_Sheet1.PrintInfo.ZoomFactor = 1f;
		this.FpSpread1_Sheet1.RowHeader.Visible = false;
		this.FpSpread1_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
		//
		//TabStrip_PreviewInfoLabel
		//
		this.TabStrip_PreviewInfoLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStrip_PreviewInfoLabel.Location = new System.Drawing.Point(8, 360);
		this.TabStrip_PreviewInfoLabel.Name = "TabStrip_PreviewInfoLabel";
		this.TabStrip_PreviewInfoLabel.Size = new System.Drawing.Size(480, 24);
		this.TabStrip_PreviewInfoLabel.TabIndex = 25;
		this.TabStrip_PreviewInfoLabel.Text = "Preview Only - Interactions will not be saved.";
		//
		//TabStrip_ContextMenu
		//
		this.TabStrip_ContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.TabStrip_ContextMenu_Reset });
		//
		//TabStrip_ContextMenu_Reset
		//
		this.TabStrip_ContextMenu_Reset.Index = 0;
		this.TabStrip_ContextMenu_Reset.Text = "Revert";
		//
		//TabStripHelpBtn
		//
		this.TabStripHelpBtn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
		this.TabStripHelpBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
		this.TabStripHelpBtn.Location = new System.Drawing.Point(408, 389);
		this.TabStripHelpBtn.Name = "TabStripHelpBtn";
		this.TabStripHelpBtn.Size = new System.Drawing.Size(80, 24);
		this.TabStripHelpBtn.TabIndex = 18;
		this.TabStripHelpBtn.Text = "Help";
		//
		//TabStripDlg
		//
		this.AcceptButton = this.TabStripOKBtn;
		this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
		this.CancelButton = this.TabStripCancelBtn;
		this.ClientSize = new System.Drawing.Size(496, 430);
		this.Controls.AddRange(new System.Windows.Forms.Control[] {
			this.TabStripHelpBtn,
			this.TabStrip_PreviewInfoLabel,
			this.TabStrip_GBPreview,
			this.TabStrip_TabControl,
			this.TabStripApplyBtn,
			this.TabStripCancelBtn,
			this.TabStripOKBtn
		});
		this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
		//this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
		this.MaximizeBox = false;
		this.Name = "TabStripDlg";
		this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
		this.Text = "TabStrip Editor";
		this.TabStrip_TabControl.ResumeLayout(false);
		this.TabStrip_TabsGeneral.ResumeLayout(false);
		((System.ComponentModel.ISupportInitialize)this.TabStrip_Ratio).EndInit();
		this.TabStrip_TabsDefault.ResumeLayout(false);
		this.TabStrip_TabsActive.ResumeLayout(false);
		this.TabStrip_GBPreview.ResumeLayout(false);
		((System.ComponentModel.ISupportInitialize)this.FpSpread1).EndInit();
		((System.ComponentModel.ISupportInitialize)this.FpSpread1_Sheet1).EndInit();
		this.ResumeLayout(false);

	}

	#endregion
	//#99937084 Leon 20131224

	string str_ColorNotSet = "Not Set";

	private void InitValues()
	{
		if ((rm != null)) {
			TabStrip_PreviewInfoLabel.Text = rm.GetString("TabStrip_Preview_Info");
			TabStripApplyBtn.Text = rm.GetString("TabStripApplyBtn");
			TabStripCancelBtn.Text = rm.GetString("TabStripCancelBtn");
			TabStripOKBtn.Text = rm.GetString("TabStripOKBtn");
			TabStrip_TabsGeneral.Text = rm.GetString("TabStrip_TabsGeneral");
			TabStrip_Editable.Text = rm.GetString("TabStrip_Editable");
			TabStrip_RatioLabel.Text = rm.GetString("TabStrip_RatioLabel");
			TabStrip_BackColorLabel.Text = rm.GetString("TabStrip_BackColorLabel");
			TabStrip_TabStripButtonPolicyLabel.Text = rm.GetString("TabStrip_TabStripButtonPolicyLabel");
			TabStrip_TabStripPolicyLabel.Text = rm.GetString("TabStrip_TabStripPolicyLabel");
			TabStrip_TabStripPlacementLabel.Text = rm.GetString("TabStrip_TabStripPlacementLabel");
			TabStrip_TabsDefault.Text = rm.GetString("TabStrip_TabsDefault");
			TabStrip_DefaultGBFont.Text = rm.GetString("TabStrip_DefaultGBFont");
			TabStrip_DefaultTabSizeLabel.Text = rm.GetString("TabStrip_DefaultTabSizeLabel");
			TabStrip_DefaultForeColorLabel.Text = rm.GetString("TabStrip_DefaultForeColorLabel");
			TabStrip_DefaultBackColorLabel.Text = rm.GetString("TabStrip_DefaultBackColorLabel");
			TabStrip_TabsActive.Text = rm.GetString("TabStrip_TabsActive");
			TabStrip_ActiveTabSizeLabel.Text = rm.GetString("TabStrip_ActiveTabSizeLabel");
			TabStrip_ActiveForeColorLabel.Text = rm.GetString("TabStrip_ActiveForeColorLabel");
			TabStrip_ActiveBackColorLabel.Text = rm.GetString("TabStrip_ActiveBackColorLabel");
			TabStrip_GBPreview.Text = rm.GetString("TabStrip_GBPreview");
			TabStrip_ActiveGBFont.Text = rm.GetString("TabStrip_ActiveGBFont");
			TabStrip_ContextMenu_Reset.Text = rm.GetString("TabStrip_ContextMenu_Reset");
			TabStripHelpBtn.Text = rm.GetString("ct_HelpButton");
			this.Text = rm.GetString("TabStrip_Editor");
			//#99937084 Leon 20131224
			str_ColorNotSet = rm.GetString("Color_NotSet");

		}

		// add fontPicker controls
		activeFontPicker = new FarPoint.Win.Spread.DrawingSpace.FontPicker(false, true);
		activeFontPicker.Location = new System.Drawing.Point(18, 18);
		activeFontPicker.Name = "activeFontPicker";
		activeFontPicker.Size = new System.Drawing.Size(300, 80);
		//99902260
		activeFontPicker.TabIndex = 10;
		activeFontPicker.TabStop = true;
		activeFontPicker.Visible = true;
		activeFontPicker.FontSelected += activeFontPicker_FontChanged;
		this.TabStrip_ActiveGBFont.Controls.Add(activeFontPicker);

		defaultFontPicker = new FarPoint.Win.Spread.DrawingSpace.FontPicker(false, true);
		defaultFontPicker.Location = new System.Drawing.Point(18, 18);
		defaultFontPicker.Name = "defaultFontPicker";
		defaultFontPicker.Size = new System.Drawing.Size(300, 80);
		//99902260
		defaultFontPicker.TabIndex = 10;
		defaultFontPicker.TabStop = true;
		defaultFontPicker.Visible = true;
		defaultFontPicker.FontSelected += defaultFontPicker_FontChanged;
		this.TabStrip_DefaultGBFont.Controls.Add(defaultFontPicker);

		InputMap normalAncestorMap = FpSpread1.GetInputMap(InputMapMode.WhenAncestorOfFocused, OperationMode.Normal);
		normalAncestorMap.Put(new Keystroke(Keys.Tab, Keys.None), SpreadActions.None);
		normalAncestorMap.Put(new Keystroke(Keys.Tab, Keys.Shift), SpreadActions.None);

		TabStrip_TabStripPlacement.Items.Clear();
		TabStrip_TabStripPlacement.Items.AddRange(new object[] {
			rm.GetString("TabStrip_Placement1"),
			rm.GetString("TabStrip_Placement2"),
			rm.GetString("TabStrip_Placement3")
		});

		TabStrip_TabStripPolicy.Items.Clear();
		TabStrip_TabStripPolicy.Items.AddRange(new object[] {
			rm.GetString("TabStrip_TabStripPolicy1"),
			rm.GetString("TabStrip_TabStripPolicy2"),
			rm.GetString("TabStrip_TabStripPolicy3")
		});

		TabStrip_TabStripButtonPolicy.Items.Clear();
		TabStrip_TabStripButtonPolicy.Items.AddRange(new object[] {
			rm.GetString("TabStrip_ButtonPolicy1"),
			rm.GetString("TabStrip_ButtonPolicy2"),
			rm.GetString("TabStrip_ButtonPolicy3")
		});

		if ((theControl == null)) {
			return;
		}

		TabStrip_TabStripPlacement.SelectedIndex =(int) theControl.TabStripPlacement;
		TabStrip_TabStripPolicy.SelectedIndex = (int)theControl.TabStripPolicy;
		TabStrip_TabStripButtonPolicy.SelectedIndex = (int)theControl.TabStrip.ButtonPolicy;
		TabStrip_Editable.Checked = theControl.TabStrip.Editable;
    TabStrip_Ratio.Value = (decimal)theControl.TabStripRatio * 100;
		if ((theControl.TabStrip.BackColor.IsEmpty)) {
			//#99937084 Leon 20131224
			TabStrip_BackColor.Text = str_ColorNotSet;

		} else {
			TabStrip_BackColor.BackColor = theControl.TabStrip.BackColor;
		}

		if ((theControl.TabStrip.ActiveSheetTab.Font == null)) {
			activeFontPicker.SelectedFont = Control.DefaultFont;
		} else {
			activeFontPicker.SelectedFont = theControl.TabStrip.ActiveSheetTab.Font;
		}

		if ((theControl.TabStrip.DefaultSheetTab.Font == null)) {
			defaultFontPicker.SelectedFont = Control.DefaultFont;
		} else {
			defaultFontPicker.SelectedFont = theControl.TabStrip.DefaultSheetTab.Font;
		}
		FpSpread1.TabStripInsertTab = false;
		FpSpread1.InterfaceRenderer = theControl.InterfaceRenderer;
		FpSpread1.ActiveSheet.VisualStyles = theControl.ActiveSheet.VisualStyles;
		FpSpread1.Sheets.Count = theControl.TabStrip.Count;
		FpSpread1.TabStrip.BackColor = theControl.TabStrip.BackColor;
		FpSpread1.TabStrip.Editable = theControl.TabStrip.Editable;
		FpSpread1.TabStrip.ButtonPolicy = theControl.TabStrip.ButtonPolicy;
		FpSpread1.TabStripPolicy = theControl.TabStripPolicy;
		FpSpread1.TabStripPlacement = theControl.TabStripPlacement;
		FpSpread1.TabStripRatio = theControl.TabStripRatio;

		foreach (SheetView sv in  FpSpread1.Sheets) 
    {
			sv.ColumnHeaderVisible = false;
			sv.RowHeaderVisible = false;
		}

		//Dim bnd As New Binding("Text", FpSpread1.GetRootWorkbook(), "TabStripRatio")
		//AddHandler bnd.Format, AddressOf binding_Format
		//AddHandler bnd.Parse, AddressOf binding_Parse
		//TabStrip_Ratio.DataBindings.Add(bnd)

		TabStrip_DefaultBackColor.ContextMenu = TabStrip_ContextMenu;
		TabStrip_DefaultForeColor.ContextMenu = TabStrip_ContextMenu;
		TabStrip_ActiveBackColor.ContextMenu = TabStrip_ContextMenu;
		TabStrip_ActiveForeColor.ContextMenu = TabStrip_ContextMenu;
		TabStrip_BackColor.ContextMenu = TabStrip_ContextMenu;

		if ((theControl.TabStrip.ActiveSheetTab.ForeColor.IsEmpty)) {
			//#99937084 Leon 20131224
			TabStrip_ActiveForeColor.Text = str_ColorNotSet;

		} else {
			TabStrip_ActiveForeColor.BackColor = theControl.TabStrip.ActiveSheetTab.ForeColor;
		}

		if ((theControl.TabStrip.ActiveSheetTab.BackColor.IsEmpty)) {
			//#99937084 Leon 20131224
			TabStrip_ActiveBackColor.Text = str_ColorNotSet;
		} else {
			TabStrip_ActiveBackColor.BackColor = theControl.TabStrip.ActiveSheetTab.BackColor;
		}

		if ((theControl.TabStrip.DefaultSheetTab.ForeColor.IsEmpty)) {
			//#99937084 Leon 20131224
			TabStrip_DefaultForeColor.Text = str_ColorNotSet;

		} else {
			TabStrip_DefaultForeColor.BackColor = theControl.TabStrip.DefaultSheetTab.ForeColor;
		}

		if ((theControl.TabStrip.DefaultSheetTab.BackColor.IsEmpty)) {
			//#99937084 Leon 20131224
			TabStrip_DefaultBackColor.Text = str_ColorNotSet;
		} else {
			TabStrip_DefaultBackColor.BackColor = theControl.TabStrip.DefaultSheetTab.BackColor;
		}

		FpSpread1.TabStrip.ActiveSheetTab.BackColor = theControl.TabStrip.ActiveSheetTab.BackColor;
		FpSpread1.TabStrip.ActiveSheetTab.ForeColor = theControl.TabStrip.ActiveSheetTab.ForeColor;
		FpSpread1.TabStrip.ActiveSheetTab.Font = theControl.TabStrip.ActiveSheetTab.Font;
		FpSpread1.TabStrip.ActiveSheetTab.Size = theControl.TabStrip.ActiveSheetTab.Size;

		FpSpread1.TabStrip.DefaultSheetTab.BackColor = theControl.TabStrip.DefaultSheetTab.BackColor;
		FpSpread1.TabStrip.DefaultSheetTab.ForeColor = theControl.TabStrip.DefaultSheetTab.ForeColor;
		FpSpread1.TabStrip.DefaultSheetTab.Font = theControl.TabStrip.DefaultSheetTab.Font;
		FpSpread1.TabStrip.DefaultSheetTab.Size = theControl.TabStrip.DefaultSheetTab.Size;

		FpSpread1.RightToLeft = theControl.RightToLeft;

		TabStrip_DefaultTabSize.Text = FpSpread1.TabStrip.DefaultSheetTab.Size.ToString();
		TabStrip_ActiveTabSize.Text = FpSpread1.TabStrip.ActiveSheetTab.Size.ToString();

		FpSpread1.ActiveSheetIndex = theControl.ActiveSheetIndex;
		FpSpread1.VisualStyles = theControl.VisualStyles;
		TabStripApplyBtn.Enabled = false;
	}

	private void ApplyChanges()
	{
		if ((theControl == null)) {
			return;
		}

    theControl.TabStripPlacement = (TabStripPlacement)TabStrip_TabStripPlacement.SelectedIndex;
    theControl.TabStripPolicy = (TabStripPolicy)TabStrip_TabStripPolicy.SelectedIndex;
    theControl.TabStrip.ButtonPolicy = (TabStripButtonPolicy)TabStrip_TabStripButtonPolicy.SelectedIndex;
		theControl.TabStrip.Editable = TabStrip_Editable.Checked;
    theControl.TabStripRatio = (double)TabStrip_Ratio.Value / 100;
		//#99937084 Leon 20131224

		if ((TabStrip_BackColor.Text == str_ColorNotSet)) {
			theControl.TabStrip.BackColor = System.Drawing.Color.Empty;
		} else {
			theControl.TabStrip.BackColor = TabStrip_BackColor.BackColor;
		}

		theControl.TabStrip.ActiveSheetTab.BackColor = FpSpread1.TabStrip.ActiveSheetTab.BackColor;
		theControl.TabStrip.ActiveSheetTab.ForeColor = FpSpread1.TabStrip.ActiveSheetTab.ForeColor;
		theControl.TabStrip.ActiveSheetTab.Font = FpSpread1.TabStrip.ActiveSheetTab.Font;
		theControl.TabStrip.ActiveSheetTab.Size = FpSpread1.TabStrip.ActiveSheetTab.Size;

		theControl.TabStrip.DefaultSheetTab.BackColor = FpSpread1.TabStrip.DefaultSheetTab.BackColor;
		theControl.TabStrip.DefaultSheetTab.ForeColor = FpSpread1.TabStrip.DefaultSheetTab.ForeColor;
		theControl.TabStrip.DefaultSheetTab.Font = FpSpread1.TabStrip.DefaultSheetTab.Font;
		theControl.TabStrip.DefaultSheetTab.Size = FpSpread1.TabStrip.DefaultSheetTab.Size;
		//((DesignerMain)theControl.FindForm()).SetDirty(true);
	}

	private void TabStripOKBtn_Click(System.Object sender, System.EventArgs e)
	{
		ApplyChanges();
	}

	private void TabStripApplyBtn_Click(System.Object sender, System.EventArgs e)
	{
		ApplyChanges();
		TabStripApplyBtn.Enabled = false;
	}

	private void TabStrip_TabStripPlacement_SelectedIndexChanged(System.Object sender, System.EventArgs e)
	{
    FpSpread1.TabStripPlacement = (TabStripPlacement)TabStrip_TabStripPlacement.SelectedIndex;
		if ((FpSpread1.TabStripPlacement == TabStripPlacement.Bottom)) {
			FpSpread1.HorizontalScrollBarPolicy = ScrollBarPolicy.Never;
		} else {
			FpSpread1.HorizontalScrollBarPolicy = ScrollBarPolicy.Always;
		}
		TabStripApplyBtn.Enabled = true;
	}

	private void TabStrip_TabStripPolicy_SelectedIndexChanged(System.Object sender, System.EventArgs e)
	{
    FpSpread1.TabStripPolicy = (TabStripPolicy)TabStrip_TabStripPolicy.SelectedIndex;
		TabStripApplyBtn.Enabled = true;
	}

	private void TabStrip_TabStripButtonPolicy_SelectedIndexChanged(System.Object sender, System.EventArgs e)
	{
    FpSpread1.TabStrip.ButtonPolicy = (TabStripButtonPolicy)TabStrip_TabStripButtonPolicy.SelectedIndex;
		TabStripApplyBtn.Enabled = true;
	}

	private void TabStrip_Ratio_TextChanged(System.Object sender, System.EventArgs e)
	{
		try {
      FpSpread1.TabStripRatio = (double)TabStrip_Ratio.Value / 100;
			TabStripApplyBtn.Enabled = true;

		} catch (Exception ex) {
		}
	}

	private void TabStrip_Editable_CheckedChanged(System.Object sender, System.EventArgs e)
	{
		FpSpread1.TabStrip.Editable = TabStrip_Editable.Checked;
		TabStripApplyBtn.Enabled = true;
	}

	private void ColorSelect_Click(System.Object sender, System.EventArgs e)
	{
		ColorDialog colorDialog1 = new ColorDialog();
		colorDialog1.AllowFullOpen = true;
		colorDialog1.SolidColorOnly = true;
		colorDialog1.FullOpen = true;

		switch ((((Button)sender).Name)) {
			case "TabStrip_BackColorSelect":
				colorDialog1.Color = TabStrip_BackColor.BackColor;
				break;
			case "TabStrip_ActiveForeColorSelect":
				colorDialog1.Color = TabStrip_ActiveForeColor.BackColor;
				break;
			case "TabStrip_ActiveBackColorSelect":
				colorDialog1.Color = TabStrip_ActiveBackColor.BackColor;
				break;
			case "TabStrip_DefaultForeColorSelect":
				colorDialog1.Color = TabStrip_DefaultForeColor.BackColor;
				break;
			case "TabStrip_DefaultBackColorSelect":
				colorDialog1.Color = TabStrip_DefaultBackColor.BackColor;

				break;
		}

		if ((colorDialog1.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)) {
			return;
		}

		switch ((((Button)sender).Name)) {
			case "TabStrip_BackColorSelect":
				TabStrip_BackColor.BackColor = colorDialog1.Color;
				TabStrip_BackColor.Text = null;
				FpSpread1.TabStrip.BackColor = TabStrip_BackColor.BackColor;
				break;
			case "TabStrip_ActiveForeColorSelect":
				TabStrip_ActiveForeColor.BackColor = colorDialog1.Color;
				TabStrip_ActiveForeColor.Text = null;
				FpSpread1.TabStrip.ActiveSheetTab.ForeColor = TabStrip_ActiveForeColor.BackColor;
				break;
			case "TabStrip_ActiveBackColorSelect":
				TabStrip_ActiveBackColor.BackColor = colorDialog1.Color;
				TabStrip_ActiveBackColor.Text = null;
				FpSpread1.TabStrip.ActiveSheetTab.BackColor = TabStrip_ActiveBackColor.BackColor;
				break;
			case "TabStrip_DefaultForeColorSelect":
				TabStrip_DefaultForeColor.BackColor = colorDialog1.Color;
				TabStrip_DefaultForeColor.Text = null;
				FpSpread1.TabStrip.DefaultSheetTab.ForeColor = TabStrip_DefaultForeColor.BackColor;
				break;
			case "TabStrip_DefaultBackColorSelect":
				TabStrip_DefaultBackColor.BackColor = colorDialog1.Color;
				TabStrip_DefaultBackColor.Text = null;
				FpSpread1.TabStrip.DefaultSheetTab.BackColor = TabStrip_DefaultBackColor.BackColor;
				break;
		}

		TabStripApplyBtn.Enabled = true;

	}
	private void TabStrip_ContextMenu_Reset_Click(System.Object sender, System.EventArgs e)
	{
		if (((((Label)TabStrip_ContextMenu.SourceControl).Tag != null) && ((System.Drawing.Color)((Label)TabStrip_ContextMenu.SourceControl).Tag).IsEmpty ==false)) 
    {
			((Label)TabStrip_ContextMenu.SourceControl).Text = null;
			((Label)TabStrip_ContextMenu.SourceControl).BackColor = (System.Drawing.Color)((Label)TabStrip_ContextMenu.SourceControl).Tag;
			if ((object.ReferenceEquals(TabStrip_ActiveForeColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.ActiveSheetTab.ForeColor = TabStrip_ActiveForeColor.BackColor;
			} else if ((object.ReferenceEquals(TabStrip_ActiveBackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.ActiveSheetTab.BackColor = TabStrip_ActiveBackColor.BackColor;
			} else if ((object.ReferenceEquals(TabStrip_DefaultForeColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.DefaultSheetTab.ForeColor = TabStrip_DefaultForeColor.BackColor;
			} else if ((object.ReferenceEquals(TabStrip_DefaultBackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.DefaultSheetTab.BackColor = TabStrip_DefaultBackColor.BackColor;
			} else if ((object.ReferenceEquals(TabStrip_BackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.BackColor = TabStrip_BackColor.BackColor;
			}
		} else {
			((Label)TabStrip_ContextMenu.SourceControl).Text = rm.GetString("Color_NotSet");
			((Label)TabStrip_ContextMenu.SourceControl).BackColor = System.Drawing.SystemColors.Control;
			if ((object.ReferenceEquals(TabStrip_ActiveForeColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.ActiveSheetTab.ForeColor = System.Drawing.Color.Empty;
			} else if ((object.ReferenceEquals(TabStrip_ActiveBackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.ActiveSheetTab.BackColor = System.Drawing.Color.Empty;
			} else if ((object.ReferenceEquals(TabStrip_DefaultForeColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.DefaultSheetTab.ForeColor = System.Drawing.Color.Empty;
			} else if ((object.ReferenceEquals(TabStrip_DefaultBackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.DefaultSheetTab.BackColor = System.Drawing.Color.Empty;
			} else if ((object.ReferenceEquals(TabStrip_BackColor, TabStrip_ContextMenu.SourceControl))) {
				FpSpread1.TabStrip.BackColor = System.Drawing.Color.Empty;
			}
		}
		TabStripApplyBtn.Enabled = true;
	}

	private void TabStrip_ContextMenu_Popup(object sender, System.EventArgs e)
	{
		TabStrip_ContextMenu_Reset.Text = rm.GetString("Context_Revert");
	}

	private void activeFontPicker_FontChanged(object sender, System.EventArgs e)
	{
		FpSpread1.TabStrip.ActiveSheetTab.Font = activeFontPicker.SelectedFont;
		TabStripApplyBtn.Enabled = true;
	}

	private void defaultFontPicker_FontChanged(object sender, System.EventArgs e)
	{
		FpSpread1.TabStrip.DefaultSheetTab.Font = defaultFontPicker.SelectedFont;
		TabStripApplyBtn.Enabled = true;
	}

	//Private Sub binding_Format(ByVal sender As Object, ByVal e As ConvertEventArgs)
	//  e.Value = e.Value * 100
	//End Sub

	//Private Sub binding_Parse(ByVal sender As Object, ByVal e As ConvertEventArgs)
	//  e.Value = e.Value / 100
	//End Sub

	private void TabStrip_ActiveTabSize_TextChanged(object sender, System.EventArgs e)
	{
		try {
			if ((TabStrip_ActiveTabSize.Text.Length > 0)) {
				if ((TabStrip_ActiveTabSize.Text != "-")) {
					FpSpread1.TabStrip.ActiveSheetTab.Size = Convert.ToInt32(TabStrip_ActiveTabSize.Text);
					TabStrip_ActiveTabSize.Text = FpSpread1.TabStrip.ActiveSheetTab.Size.ToString();
				}
			}
		} catch {
			TabStrip_ActiveTabSize.Text = FpSpread1.TabStrip.ActiveSheetTab.Size.ToString();
			TabStrip_ActiveTabSize.SelectAll();
			if ((rm != null)) {
				MessageBox.Show(rm.GetString("TabStrip_Size_Error"), rm.GetString("DesignerErrorTitle"));
			}
		}
		TabStripApplyBtn.Enabled = true;
	}

	private void TabStrip_DefaultTabSize_TextChanged(object sender, System.EventArgs e)
	{
		try {
			if ((TabStrip_DefaultTabSize.Text.Length > 0)) {
				if ((TabStrip_DefaultTabSize.Text != "-")) {
					FpSpread1.TabStrip.DefaultSheetTab.Size = Convert.ToInt32(TabStrip_DefaultTabSize.Text);
					TabStrip_DefaultTabSize.Text = FpSpread1.TabStrip.DefaultSheetTab.Size.ToString();
				}
			}
		} catch {
			TabStrip_DefaultTabSize.Text = FpSpread1.TabStrip.DefaultSheetTab.Size.ToString();
			TabStrip_DefaultTabSize.SelectAll();
			if ((rm != null)) {
				MessageBox.Show(rm.GetString("TabStrip_Size_Error"), rm.GetString("DesignerErrorTitle"));
			}
		}
		TabStripApplyBtn.Enabled = true;
	}

	
	private void TabStripDlg_Load(object sender, System.EventArgs e)
	{
		if ((this.Owner != null))
			this.Icon = this.Owner.Icon;
	}

	private void TabStripHelpBtn_Click(System.Object sender, System.EventArgs e)
	{
		
	}
}
}
