﻿using FarPoint.Win.Spread;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace SpreadDesigner
{
  public class PrintOptionsDlg : System.Windows.Forms.Form
  {

    System.Resources.ResourceSet rm = null;
    // Variable to represent the spreadsheet being designed
    private FarPoint.Win.Spread.FpSpread SpreadWrapper1;
    // Variable containing the collection of Smart Print Rules
    private SmartPrintRulesCollection rulesCollection;
    // Variable containing the ZoomFactor value
    private float zoomFactor;
    // Variables for th miscellaneous margins for printing
    private float marginsTop;
    private float marginsLeft;
    private float marginsRight;
    private float marginsBottom;
    private float marginsHeader;

    private float marginsFooter;

    public PrintOptionsDlg(FarPoint.Win.Spread.FpSpread spread, int tabIndex)
      : base()
    {
      if (FarPoint.Win.Spread.Design.common.rm != null)
      {
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      else
      {
        FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
        rm = FarPoint.Win.Spread.Design.common.rm;
      }

      KeyDown += PrintOptionsDlg_KeyDown;
      Load += PrintOptionsDlg_Load;
      // Create the SmartPrint Rules collection
      rulesCollection = new SmartPrintRulesCollection();
      // Store the spreadsheet control being modified
      this.SpreadWrapper1 = spread;


      //This call is required by the Windows Form Designer.
      InitializeComponent();
      TabControl1.SelectedIndex = tabIndex;

    }


    public PrintOptionsDlg(FarPoint.Win.Spread.FpSpread spread)
      : base()
    {
      if (FarPoint.Win.Spread.Design.common.rm != null)
      {
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      else
      {
        FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
        rm = FarPoint.Win.Spread.Design.common.rm;
      }

      KeyDown += PrintOptionsDlg_KeyDown;
      Load += PrintOptionsDlg_Load;
      // Create the SmartPrint Rules collection
      rulesCollection = new SmartPrintRulesCollection();
      // Store the spreadsheet control being modified
      this.SpreadWrapper1 = spread;

      //This call is required by the Windows Form Designer.
      InitializeComponent();

    }

    #region " Windows Form Designer generated code "

    public PrintOptionsDlg()
      : base()
    {
      KeyDown += PrintOptionsDlg_KeyDown;
      Load += PrintOptionsDlg_Load;

      //This call is required by the Windows Form Designer.
      InitializeComponent();

      //Add any initialization after the InitializeComponent() call

    }

    //Form overrides dispose to clean up the component list.
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if ((components != null))
        {
          components.Dispose();
        }
      }
      base.Dispose(disposing);
    }

    //Required by the Windows Form Designer

    private System.ComponentModel.IContainer components;
    //NOTE: The following procedure is required by the Windows Form Designer
    //It can be modified using the Windows Form Designer.  
    //Do not modify it using the code editor.
    protected internal System.Windows.Forms.TabControl TabControl1;
    protected internal System.Windows.Forms.Panel Panel1;
    protected System.Windows.Forms.TabPage prt_TabGeneral;
    protected System.Windows.Forms.CheckBox prt_Gen_UseMax;
    protected System.Windows.Forms.TextBox prt_Gen_AbortMessage;
    protected System.Windows.Forms.Label prt_Gen_AbortMsgLabel;
    protected System.Windows.Forms.TextBox prt_Gen_JobName;
    protected System.Windows.Forms.Label prt_Gen_PrintJobLabel;
    protected System.Windows.Forms.TextBox prt_Gen_Printer;
    protected System.Windows.Forms.Label prt_Gen_PrinterNameLabel;
    protected System.Windows.Forms.CheckBox prt_Gen_Preview;
    protected System.Windows.Forms.TabPage prt_TabOutput;
    protected System.Windows.Forms.TabPage prt_TabHeadFoot;
    protected System.Windows.Forms.TabPage prt_TabSmartPrint;
    protected System.Windows.Forms.TabPage prt_TabPagination;
    protected System.Windows.Forms.TabPage prt_TabMargins;
    protected System.Windows.Forms.Label prt_Gen_ZoomPctLabel;
    protected System.Windows.Forms.CheckBox prt_Gen_BestFitRows;
    protected System.Windows.Forms.CheckBox prt_Gen_BestFitCols;
    private System.Windows.Forms.TextBox withEventsField_prt_Gen_ZoomFactor;
    protected System.Windows.Forms.TextBox prt_Gen_ZoomFactor
    {
      get { return withEventsField_prt_Gen_ZoomFactor; }
      set
      {
        if (withEventsField_prt_Gen_ZoomFactor != null)
        {
          withEventsField_prt_Gen_ZoomFactor.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Gen_ZoomFactor = value;
        if (withEventsField_prt_Gen_ZoomFactor != null)
        {
          withEventsField_prt_Gen_ZoomFactor.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Gen_ZoomLabel;
    protected System.Windows.Forms.ComboBox prt_Out_PrintType;
    protected System.Windows.Forms.Label prt_Out_TypeLabel;
    protected System.Windows.Forms.CheckBox prt_Out_ShowColumnHeaders;
    protected System.Windows.Forms.CheckBox prt_Out_ShowColor;
    protected System.Windows.Forms.CheckBox prt_Out_ShowBorder;
    private System.Windows.Forms.Button withEventsField_prt_CancelButton;
    protected System.Windows.Forms.Button prt_CancelButton
    {
      get { return withEventsField_prt_CancelButton; }
      set
      {
        if (withEventsField_prt_CancelButton != null)
        {
          withEventsField_prt_CancelButton.Click -= Cancel_Click;
        }
        withEventsField_prt_CancelButton = value;
        if (withEventsField_prt_CancelButton != null)
        {
          withEventsField_prt_CancelButton.Click += Cancel_Click;
        }
      }
    }
    private System.Windows.Forms.Button withEventsField_prt_OKButton;
    protected System.Windows.Forms.Button prt_OKButton
    {
      get { return withEventsField_prt_OKButton; }
      set
      {
        if (withEventsField_prt_OKButton != null)
        {
          withEventsField_prt_OKButton.Click -= OK_Click;
        }
        withEventsField_prt_OKButton = value;
        if (withEventsField_prt_OKButton != null)
        {
          withEventsField_prt_OKButton.Click += OK_Click;
        }
      }
    }
    protected System.Windows.Forms.ComboBox prt_Out_Orientation;
    protected System.Windows.Forms.Label prt_Out_OrientationLabel;
    protected System.Windows.Forms.CheckBox prt_Out_ShowPrintDialog;
    protected System.Windows.Forms.CheckBox prt_Out_ShowShadows;
    protected System.Windows.Forms.CheckBox prt_Out_ShowGrid;
    protected System.Windows.Forms.CheckBox prt_Out_ShowRowHeaders;
    protected System.Windows.Forms.TextBox prt_HeadFoot_FooterText;
    private System.Windows.Forms.RadioButton withEventsField_prt_HeadFoot_Footer;
    protected System.Windows.Forms.RadioButton prt_HeadFoot_Footer
    {
      get { return withEventsField_prt_HeadFoot_Footer; }
      set
      {
        if (withEventsField_prt_HeadFoot_Footer != null)
        {
          withEventsField_prt_HeadFoot_Footer.CheckedChanged -= HeadFoot_Footer_CheckedChanged;
        }
        withEventsField_prt_HeadFoot_Footer = value;
        if (withEventsField_prt_HeadFoot_Footer != null)
        {
          withEventsField_prt_HeadFoot_Footer.CheckedChanged += HeadFoot_Footer_CheckedChanged;
        }
      }
    }
    private System.Windows.Forms.RadioButton withEventsField_prt_HeadFoot_Header;
    protected System.Windows.Forms.RadioButton prt_HeadFoot_Header
    {
      get { return withEventsField_prt_HeadFoot_Header; }
      set
      {
        if (withEventsField_prt_HeadFoot_Header != null)
        {
          withEventsField_prt_HeadFoot_Header.CheckedChanged -= HeadFoot_Header_CheckedChanged;
        }
        withEventsField_prt_HeadFoot_Header = value;
        if (withEventsField_prt_HeadFoot_Header != null)
        {
          withEventsField_prt_HeadFoot_Header.CheckedChanged += HeadFoot_Header_CheckedChanged;
        }
      }
    }
    private System.Windows.Forms.Button withEventsField_prt_HeadFoot_AddButton;
    protected System.Windows.Forms.Button prt_HeadFoot_AddButton
    {
      get { return withEventsField_prt_HeadFoot_AddButton; }
      set
      {
        if (withEventsField_prt_HeadFoot_AddButton != null)
        {
          withEventsField_prt_HeadFoot_AddButton.Click -= HeadFoot_Add_Clicked;
        }
        withEventsField_prt_HeadFoot_AddButton = value;
        if (withEventsField_prt_HeadFoot_AddButton != null)
        {
          withEventsField_prt_HeadFoot_AddButton.Click += HeadFoot_Add_Clicked;
        }
      }
    }
    protected System.Windows.Forms.ComboBox prt_HeadFoot_ControlChars;
    protected System.Windows.Forms.Label prt_HeadFoot_ControlCharsLabel;
    private System.Windows.Forms.Button withEventsField_prt_SmartPrint_DeleteButton;
    protected System.Windows.Forms.Button prt_SmartPrint_DeleteButton
    {
      get { return withEventsField_prt_SmartPrint_DeleteButton; }
      set
      {
        if (withEventsField_prt_SmartPrint_DeleteButton != null)
        {
          withEventsField_prt_SmartPrint_DeleteButton.Click -= SmartPrint_Delete_Click;
        }
        withEventsField_prt_SmartPrint_DeleteButton = value;
        if (withEventsField_prt_SmartPrint_DeleteButton != null)
        {
          withEventsField_prt_SmartPrint_DeleteButton.Click += SmartPrint_Delete_Click;
        }
      }
    }
    protected System.Windows.Forms.ListBox prt_SmartPrint_RulesCollection;
    private System.Windows.Forms.TextBox withEventsField_prt_SmartPrint_ScaleInterval;
    protected System.Windows.Forms.TextBox prt_SmartPrint_ScaleInterval
    {
      get { return withEventsField_prt_SmartPrint_ScaleInterval; }
      set
      {
        if (withEventsField_prt_SmartPrint_ScaleInterval != null)
        {
          withEventsField_prt_SmartPrint_ScaleInterval.Validating -= Float_TextChanged;
        }
        withEventsField_prt_SmartPrint_ScaleInterval = value;
        if (withEventsField_prt_SmartPrint_ScaleInterval != null)
        {
          withEventsField_prt_SmartPrint_ScaleInterval.Validating += Float_TextChanged;
        }
      }
    }
    private System.Windows.Forms.TextBox withEventsField_prt_SmartPrint_ScaleEndFactor;
    protected System.Windows.Forms.TextBox prt_SmartPrint_ScaleEndFactor
    {
      get { return withEventsField_prt_SmartPrint_ScaleEndFactor; }
      set
      {
        if (withEventsField_prt_SmartPrint_ScaleEndFactor != null)
        {
          withEventsField_prt_SmartPrint_ScaleEndFactor.Validating -= Float_TextChanged;
        }
        withEventsField_prt_SmartPrint_ScaleEndFactor = value;
        if (withEventsField_prt_SmartPrint_ScaleEndFactor != null)
        {
          withEventsField_prt_SmartPrint_ScaleEndFactor.Validating += Float_TextChanged;
        }
      }
    }
    private System.Windows.Forms.TextBox withEventsField_prt_SmartPrint_ScaleStartFactor;
    protected System.Windows.Forms.TextBox prt_SmartPrint_ScaleStartFactor
    {
      get { return withEventsField_prt_SmartPrint_ScaleStartFactor; }
      set
      {
        if (withEventsField_prt_SmartPrint_ScaleStartFactor != null)
        {
          withEventsField_prt_SmartPrint_ScaleStartFactor.Validating -= Float_TextChanged;
        }
        withEventsField_prt_SmartPrint_ScaleStartFactor = value;
        if (withEventsField_prt_SmartPrint_ScaleStartFactor != null)
        {
          withEventsField_prt_SmartPrint_ScaleStartFactor.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_SmartPrint_IntervalLabel;
    protected System.Windows.Forms.Label prt_SmartPrint_EndLabel;
    protected System.Windows.Forms.Label prt_SmartPrint_StartLabel;
    protected System.Windows.Forms.ComboBox prt_SmartPrint_ResetOption;
    protected System.Windows.Forms.Label prt_SmartPrint_ResetLabel;
    private System.Windows.Forms.Label withEventsField_prt_SmartPrint_OptionsLabel;
    protected System.Windows.Forms.Label prt_SmartPrint_OptionsLabel
    {
      get { return withEventsField_prt_SmartPrint_OptionsLabel; }
      set
      {
        if (withEventsField_prt_SmartPrint_OptionsLabel != null)
        {
          withEventsField_prt_SmartPrint_OptionsLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_SmartPrint_OptionsLabel = value;
        if (withEventsField_prt_SmartPrint_OptionsLabel != null)
        {
          withEventsField_prt_SmartPrint_OptionsLabel.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.Button withEventsField_prt_SmartPrint_AddButton;
    protected System.Windows.Forms.Button prt_SmartPrint_AddButton
    {
      get { return withEventsField_prt_SmartPrint_AddButton; }
      set
      {
        if (withEventsField_prt_SmartPrint_AddButton != null)
        {
          withEventsField_prt_SmartPrint_AddButton.Click -= SmartPrint_Add_Click;
        }
        withEventsField_prt_SmartPrint_AddButton = value;
        if (withEventsField_prt_SmartPrint_AddButton != null)
        {
          withEventsField_prt_SmartPrint_AddButton.Click += SmartPrint_Add_Click;
        }
      }
    }
    private System.Windows.Forms.ComboBox withEventsField_prt_SmartPrint_Rule;
    protected System.Windows.Forms.ComboBox prt_SmartPrint_Rule
    {
      get { return withEventsField_prt_SmartPrint_Rule; }
      set
      {
        if (withEventsField_prt_SmartPrint_Rule != null)
        {
          withEventsField_prt_SmartPrint_Rule.SelectedIndexChanged -= SmartPrint_Rule_SelectedIndexChanged;
        }
        withEventsField_prt_SmartPrint_Rule = value;
        if (withEventsField_prt_SmartPrint_Rule != null)
        {
          withEventsField_prt_SmartPrint_Rule.SelectedIndexChanged += SmartPrint_Rule_SelectedIndexChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_SmartPrint_RuleLabel;
    protected System.Windows.Forms.CheckBox prt_SmartPrint_UseSmartPrint;
    protected System.Windows.Forms.ComboBox prt_Pag_PageOrder;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_RowEnd;
    protected System.Windows.Forms.TextBox prt_Pag_RowEnd
    {
      get { return withEventsField_prt_Pag_RowEnd; }
      set
      {
        if (withEventsField_prt_Pag_RowEnd != null)
        {
          withEventsField_prt_Pag_RowEnd.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_RowEnd = value;
        if (withEventsField_prt_Pag_RowEnd != null)
        {
          withEventsField_prt_Pag_RowEnd.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_RowEndLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_RowStart;
    protected System.Windows.Forms.TextBox prt_Pag_RowStart
    {
      get { return withEventsField_prt_Pag_RowStart; }
      set
      {
        if (withEventsField_prt_Pag_RowStart != null)
        {
          withEventsField_prt_Pag_RowStart.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_RowStart = value;
        if (withEventsField_prt_Pag_RowStart != null)
        {
          withEventsField_prt_Pag_RowStart.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_RowStartLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_ColumnEnd;
    protected System.Windows.Forms.TextBox prt_Pag_ColumnEnd
    {
      get { return withEventsField_prt_Pag_ColumnEnd; }
      set
      {
        if (withEventsField_prt_Pag_ColumnEnd != null)
        {
          withEventsField_prt_Pag_ColumnEnd.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_ColumnEnd = value;
        if (withEventsField_prt_Pag_ColumnEnd != null)
        {
          withEventsField_prt_Pag_ColumnEnd.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_ColEndLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_ColumnStart;
    protected System.Windows.Forms.TextBox prt_Pag_ColumnStart
    {
      get { return withEventsField_prt_Pag_ColumnStart; }
      set
      {
        if (withEventsField_prt_Pag_ColumnStart != null)
        {
          withEventsField_prt_Pag_ColumnStart.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_ColumnStart = value;
        if (withEventsField_prt_Pag_ColumnStart != null)
        {
          withEventsField_prt_Pag_ColumnStart.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_ColStartLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_PageEnd;
    protected System.Windows.Forms.TextBox prt_Pag_PageEnd
    {
      get { return withEventsField_prt_Pag_PageEnd; }
      set
      {
        if (withEventsField_prt_Pag_PageEnd != null)
        {
          withEventsField_prt_Pag_PageEnd.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_PageEnd = value;
        if (withEventsField_prt_Pag_PageEnd != null)
        {
          withEventsField_prt_Pag_PageEnd.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_PageEndLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_PageStart;
    protected System.Windows.Forms.TextBox prt_Pag_PageStart
    {
      get { return withEventsField_prt_Pag_PageStart; }
      set
      {
        if (withEventsField_prt_Pag_PageStart != null)
        {
          withEventsField_prt_Pag_PageStart.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_PageStart = value;
        if (withEventsField_prt_Pag_PageStart != null)
        {
          withEventsField_prt_Pag_PageStart.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_PageStartLabel;
    protected System.Windows.Forms.Label prt_Pag_PageOrderLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Pag_FirstPageNumber;
    protected System.Windows.Forms.TextBox prt_Pag_FirstPageNumber
    {
      get { return withEventsField_prt_Pag_FirstPageNumber; }
      set
      {
        if (withEventsField_prt_Pag_FirstPageNumber != null)
        {
          withEventsField_prt_Pag_FirstPageNumber.Validating -= Integer_TextChanged;
        }
        withEventsField_prt_Pag_FirstPageNumber = value;
        if (withEventsField_prt_Pag_FirstPageNumber != null)
        {
          withEventsField_prt_Pag_FirstPageNumber.Validating += Integer_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Pag_FirstPageLabel;
    private System.Windows.Forms.Label withEventsField_prt_Pag_RowLabel;
    protected System.Windows.Forms.Label prt_Pag_RowLabel
    {
      get { return withEventsField_prt_Pag_RowLabel; }
      set
      {
        if (withEventsField_prt_Pag_RowLabel != null)
        {
          withEventsField_prt_Pag_RowLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_Pag_RowLabel = value;
        if (withEventsField_prt_Pag_RowLabel != null)
        {
          withEventsField_prt_Pag_RowLabel.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.Label withEventsField_prt_Pag_ColumnLabel;
    protected System.Windows.Forms.Label prt_Pag_ColumnLabel
    {
      get { return withEventsField_prt_Pag_ColumnLabel; }
      set
      {
        if (withEventsField_prt_Pag_ColumnLabel != null)
        {
          withEventsField_prt_Pag_ColumnLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_Pag_ColumnLabel = value;
        if (withEventsField_prt_Pag_ColumnLabel != null)
        {
          withEventsField_prt_Pag_ColumnLabel.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.Label withEventsField_prt_Pag_PageLabel;
    protected System.Windows.Forms.Label prt_Pag_PageLabel
    {
      get { return withEventsField_prt_Pag_PageLabel; }
      set
      {
        if (withEventsField_prt_Pag_PageLabel != null)
        {
          withEventsField_prt_Pag_PageLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_Pag_PageLabel = value;
        if (withEventsField_prt_Pag_PageLabel != null)
        {
          withEventsField_prt_Pag_PageLabel.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.Label withEventsField_prt_Pag_SettingsLabel;
    protected System.Windows.Forms.Label prt_Pag_SettingsLabel
    {
      get { return withEventsField_prt_Pag_SettingsLabel; }
      set
      {
        if (withEventsField_prt_Pag_SettingsLabel != null)
        {
          withEventsField_prt_Pag_SettingsLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_Pag_SettingsLabel = value;
        if (withEventsField_prt_Pag_SettingsLabel != null)
        {
          withEventsField_prt_Pag_SettingsLabel.Paint += Label_Paint;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_SizingNote;
    private System.Windows.Forms.Label withEventsField_prt_Margin_RegionsLabel;
    protected System.Windows.Forms.Label prt_Margin_RegionsLabel
    {
      get { return withEventsField_prt_Margin_RegionsLabel; }
      set
      {
        if (withEventsField_prt_Margin_RegionsLabel != null)
        {
          withEventsField_prt_Margin_RegionsLabel.Paint -= Label_Paint;
        }
        withEventsField_prt_Margin_RegionsLabel = value;
        if (withEventsField_prt_Margin_RegionsLabel != null)
        {
          withEventsField_prt_Margin_RegionsLabel.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Footer;
    protected System.Windows.Forms.TextBox prt_Margin_Footer
    {
      get { return withEventsField_prt_Margin_Footer; }
      set
      {
        if (withEventsField_prt_Margin_Footer != null)
        {
          withEventsField_prt_Margin_Footer.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Footer = value;
        if (withEventsField_prt_Margin_Footer != null)
        {
          withEventsField_prt_Margin_Footer.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_FooterLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Header;
    protected System.Windows.Forms.TextBox prt_Margin_Header
    {
      get { return withEventsField_prt_Margin_Header; }
      set
      {
        if (withEventsField_prt_Margin_Header != null)
        {
          withEventsField_prt_Margin_Header.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Header = value;
        if (withEventsField_prt_Margin_Header != null)
        {
          withEventsField_prt_Margin_Header.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_HeaderLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Bottom;
    protected System.Windows.Forms.TextBox prt_Margin_Bottom
    {
      get { return withEventsField_prt_Margin_Bottom; }
      set
      {
        if (withEventsField_prt_Margin_Bottom != null)
        {
          withEventsField_prt_Margin_Bottom.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Bottom = value;
        if (withEventsField_prt_Margin_Bottom != null)
        {
          withEventsField_prt_Margin_Bottom.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_BottomLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Right;
    protected System.Windows.Forms.TextBox prt_Margin_Right
    {
      get { return withEventsField_prt_Margin_Right; }
      set
      {
        if (withEventsField_prt_Margin_Right != null)
        {
          withEventsField_prt_Margin_Right.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Right = value;
        if (withEventsField_prt_Margin_Right != null)
        {
          withEventsField_prt_Margin_Right.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_RightLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Left;
    protected System.Windows.Forms.TextBox prt_Margin_Left
    {
      get { return withEventsField_prt_Margin_Left; }
      set
      {
        if (withEventsField_prt_Margin_Left != null)
        {
          withEventsField_prt_Margin_Left.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Left = value;
        if (withEventsField_prt_Margin_Left != null)
        {
          withEventsField_prt_Margin_Left.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_LeftLabel;
    private System.Windows.Forms.TextBox withEventsField_prt_Margin_Top;
    protected System.Windows.Forms.TextBox prt_Margin_Top
    {
      get { return withEventsField_prt_Margin_Top; }
      set
      {
        if (withEventsField_prt_Margin_Top != null)
        {
          withEventsField_prt_Margin_Top.Validating -= Float_TextChanged;
        }
        withEventsField_prt_Margin_Top = value;
        if (withEventsField_prt_Margin_Top != null)
        {
          withEventsField_prt_Margin_Top.Validating += Float_TextChanged;
        }
      }
    }
    protected System.Windows.Forms.Label prt_Margin_TopLabel;
    protected System.Windows.Forms.TextBox prt_HeadFoot_HeaderText;
    [System.Diagnostics.DebuggerStepThrough()]
    private void InitializeComponent()
    {
      this.TabControl1 = new System.Windows.Forms.TabControl();
      this.prt_TabGeneral = new System.Windows.Forms.TabPage();
      this.prt_Gen_ZoomPctLabel = new System.Windows.Forms.Label();
      this.prt_Gen_BestFitRows = new System.Windows.Forms.CheckBox();
      this.prt_Gen_BestFitCols = new System.Windows.Forms.CheckBox();
      this.prt_Gen_ZoomFactor = new System.Windows.Forms.TextBox();
      this.prt_Gen_ZoomLabel = new System.Windows.Forms.Label();
      this.prt_Gen_UseMax = new System.Windows.Forms.CheckBox();
      this.prt_Gen_AbortMessage = new System.Windows.Forms.TextBox();
      this.prt_Gen_AbortMsgLabel = new System.Windows.Forms.Label();
      this.prt_Gen_JobName = new System.Windows.Forms.TextBox();
      this.prt_Gen_PrintJobLabel = new System.Windows.Forms.Label();
      this.prt_Gen_Printer = new System.Windows.Forms.TextBox();
      this.prt_Gen_PrinterNameLabel = new System.Windows.Forms.Label();
      this.prt_Gen_Preview = new System.Windows.Forms.CheckBox();
      this.prt_TabSmartPrint = new System.Windows.Forms.TabPage();
      this.prt_SmartPrint_DeleteButton = new System.Windows.Forms.Button();
      this.prt_SmartPrint_RulesCollection = new System.Windows.Forms.ListBox();
      this.prt_SmartPrint_ScaleInterval = new System.Windows.Forms.TextBox();
      this.prt_SmartPrint_ScaleEndFactor = new System.Windows.Forms.TextBox();
      this.prt_SmartPrint_ScaleStartFactor = new System.Windows.Forms.TextBox();
      this.prt_SmartPrint_IntervalLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_EndLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_StartLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_ResetOption = new System.Windows.Forms.ComboBox();
      this.prt_SmartPrint_ResetLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_OptionsLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_AddButton = new System.Windows.Forms.Button();
      this.prt_SmartPrint_Rule = new System.Windows.Forms.ComboBox();
      this.prt_SmartPrint_RuleLabel = new System.Windows.Forms.Label();
      this.prt_SmartPrint_UseSmartPrint = new System.Windows.Forms.CheckBox();
      this.prt_TabOutput = new System.Windows.Forms.TabPage();
      this.prt_Out_Orientation = new System.Windows.Forms.ComboBox();
      this.prt_Out_OrientationLabel = new System.Windows.Forms.Label();
      this.prt_Out_PrintType = new System.Windows.Forms.ComboBox();
      this.prt_Out_TypeLabel = new System.Windows.Forms.Label();
      this.prt_Out_ShowPrintDialog = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowShadows = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowGrid = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowRowHeaders = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowColumnHeaders = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowColor = new System.Windows.Forms.CheckBox();
      this.prt_Out_ShowBorder = new System.Windows.Forms.CheckBox();
      this.prt_TabHeadFoot = new System.Windows.Forms.TabPage();
      this.prt_HeadFoot_FooterText = new System.Windows.Forms.TextBox();
      this.Panel1 = new System.Windows.Forms.Panel();
      this.prt_HeadFoot_Footer = new System.Windows.Forms.RadioButton();
      this.prt_HeadFoot_Header = new System.Windows.Forms.RadioButton();
      this.prt_HeadFoot_AddButton = new System.Windows.Forms.Button();
      this.prt_HeadFoot_ControlChars = new System.Windows.Forms.ComboBox();
      this.prt_HeadFoot_ControlCharsLabel = new System.Windows.Forms.Label();
      this.prt_HeadFoot_HeaderText = new System.Windows.Forms.TextBox();
      this.prt_TabPagination = new System.Windows.Forms.TabPage();
      this.prt_Pag_PageOrder = new System.Windows.Forms.ComboBox();
      this.prt_Pag_RowEnd = new System.Windows.Forms.TextBox();
      this.prt_Pag_RowEndLabel = new System.Windows.Forms.Label();
      this.prt_Pag_RowStart = new System.Windows.Forms.TextBox();
      this.prt_Pag_RowStartLabel = new System.Windows.Forms.Label();
      this.prt_Pag_ColumnEnd = new System.Windows.Forms.TextBox();
      this.prt_Pag_ColEndLabel = new System.Windows.Forms.Label();
      this.prt_Pag_ColumnStart = new System.Windows.Forms.TextBox();
      this.prt_Pag_ColStartLabel = new System.Windows.Forms.Label();
      this.prt_Pag_PageEnd = new System.Windows.Forms.TextBox();
      this.prt_Pag_PageEndLabel = new System.Windows.Forms.Label();
      this.prt_Pag_PageStart = new System.Windows.Forms.TextBox();
      this.prt_Pag_PageStartLabel = new System.Windows.Forms.Label();
      this.prt_Pag_PageOrderLabel = new System.Windows.Forms.Label();
      this.prt_Pag_FirstPageNumber = new System.Windows.Forms.TextBox();
      this.prt_Pag_FirstPageLabel = new System.Windows.Forms.Label();
      this.prt_Pag_RowLabel = new System.Windows.Forms.Label();
      this.prt_Pag_ColumnLabel = new System.Windows.Forms.Label();
      this.prt_Pag_PageLabel = new System.Windows.Forms.Label();
      this.prt_Pag_SettingsLabel = new System.Windows.Forms.Label();
      this.prt_TabMargins = new System.Windows.Forms.TabPage();
      this.prt_Margin_SizingNote = new System.Windows.Forms.Label();
      this.prt_Margin_RegionsLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Footer = new System.Windows.Forms.TextBox();
      this.prt_Margin_FooterLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Header = new System.Windows.Forms.TextBox();
      this.prt_Margin_HeaderLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Bottom = new System.Windows.Forms.TextBox();
      this.prt_Margin_BottomLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Right = new System.Windows.Forms.TextBox();
      this.prt_Margin_RightLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Left = new System.Windows.Forms.TextBox();
      this.prt_Margin_LeftLabel = new System.Windows.Forms.Label();
      this.prt_Margin_Top = new System.Windows.Forms.TextBox();
      this.prt_Margin_TopLabel = new System.Windows.Forms.Label();
      this.prt_CancelButton = new System.Windows.Forms.Button();
      this.prt_OKButton = new System.Windows.Forms.Button();
      this.TabControl1.SuspendLayout();
      this.prt_TabGeneral.SuspendLayout();
      this.prt_TabSmartPrint.SuspendLayout();
      this.prt_TabOutput.SuspendLayout();
      this.prt_TabHeadFoot.SuspendLayout();
      this.Panel1.SuspendLayout();
      this.prt_TabPagination.SuspendLayout();
      this.prt_TabMargins.SuspendLayout();
      this.SuspendLayout();
      //
      //TabControl1
      //
      this.TabControl1.Controls.Add(this.prt_TabGeneral);
      this.TabControl1.Controls.Add(this.prt_TabSmartPrint);
      this.TabControl1.Controls.Add(this.prt_TabOutput);
      this.TabControl1.Controls.Add(this.prt_TabHeadFoot);
      this.TabControl1.Controls.Add(this.prt_TabPagination);
      this.TabControl1.Controls.Add(this.prt_TabMargins);
      this.TabControl1.ItemSize = new System.Drawing.Size(80, 18);

      this.TabControl1.Location = new System.Drawing.Point(18, 19);
      this.TabControl1.Name = "TabControl1";
      this.TabControl1.SelectedIndex = 0;
      this.TabControl1.Size = new System.Drawing.Size(534, 336);
      this.TabControl1.TabIndex = 0;
      //
      //prt_TabGeneral
      //
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_ZoomPctLabel);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_BestFitRows);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_BestFitCols);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_ZoomFactor);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_ZoomLabel);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_UseMax);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_AbortMessage);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_AbortMsgLabel);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_JobName);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_PrintJobLabel);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_Printer);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_PrinterNameLabel);
      this.prt_TabGeneral.Controls.Add(this.prt_Gen_Preview);
      this.prt_TabGeneral.Location = new System.Drawing.Point(4, 22);
      this.prt_TabGeneral.Name = "prt_TabGeneral";
      this.prt_TabGeneral.Size = new System.Drawing.Size(526, 310);
      this.prt_TabGeneral.TabIndex = 0;
      this.prt_TabGeneral.Text = "General";
      this.prt_TabGeneral.UseVisualStyleBackColor = true;
      //
      //prt_Gen_ZoomPctLabel
      //
      this.prt_Gen_ZoomPctLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_ZoomPctLabel.Location = new System.Drawing.Point(178, 205);
      this.prt_Gen_ZoomPctLabel.Name = "prt_Gen_ZoomPctLabel";
      this.prt_Gen_ZoomPctLabel.Size = new System.Drawing.Size(200, 25);
      this.prt_Gen_ZoomPctLabel.TabIndex = 4;
      this.prt_Gen_ZoomPctLabel.Text = "(percent)";
      this.prt_Gen_ZoomPctLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Gen_BestFitRows
      //
      this.prt_Gen_BestFitRows.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_BestFitRows.Location = new System.Drawing.Point(280, 169);
      this.prt_Gen_BestFitRows.Name = "prt_Gen_BestFitRows";
      this.prt_Gen_BestFitRows.Size = new System.Drawing.Size(206, 27);
      this.prt_Gen_BestFitRows.TabIndex = 12;
      this.prt_Gen_BestFitRows.Text = "Best Fit &Rows";
      //
      //prt_Gen_BestFitCols
      //
      this.prt_Gen_BestFitCols.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_BestFitCols.Location = new System.Drawing.Point(280, 131);
      this.prt_Gen_BestFitCols.Name = "prt_Gen_BestFitCols";
      this.prt_Gen_BestFitCols.Size = new System.Drawing.Size(206, 28);
      this.prt_Gen_BestFitCols.TabIndex = 11;
      this.prt_Gen_BestFitCols.Text = "Best Fit &Columns";
      //
      //prt_Gen_ZoomFactor
      //
      this.prt_Gen_ZoomFactor.Location = new System.Drawing.Point(112, 205);
      this.prt_Gen_ZoomFactor.Name = "prt_Gen_ZoomFactor";
      this.prt_Gen_ZoomFactor.Size = new System.Drawing.Size(56, 20);
      this.prt_Gen_ZoomFactor.TabIndex = 3;
      //
      //prt_Gen_ZoomLabel
      //
      this.prt_Gen_ZoomLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_ZoomLabel.Location = new System.Drawing.Point(9, 205);
      this.prt_Gen_ZoomLabel.Name = "prt_Gen_ZoomLabel";
      this.prt_Gen_ZoomLabel.Size = new System.Drawing.Size(91, 25);
      this.prt_Gen_ZoomLabel.TabIndex = 2;
      this.prt_Gen_ZoomLabel.Text = "&Zoom:";
      this.prt_Gen_ZoomLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Gen_UseMax
      //
      this.prt_Gen_UseMax.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_UseMax.Location = new System.Drawing.Point(9, 150);
      this.prt_Gen_UseMax.Name = "prt_Gen_UseMax";
      this.prt_Gen_UseMax.Size = new System.Drawing.Size(262, 28);
      this.prt_Gen_UseMax.TabIndex = 1;
      this.prt_Gen_UseMax.Text = "Print Only Rows With &Data";
      //
      //prt_Gen_AbortMessage
      //
      this.prt_Gen_AbortMessage.Location = new System.Drawing.Point(280, 94);
      this.prt_Gen_AbortMessage.Name = "prt_Gen_AbortMessage";
      this.prt_Gen_AbortMessage.Size = new System.Drawing.Size(234, 20);
      this.prt_Gen_AbortMessage.TabIndex = 10;
      //
      //prt_Gen_AbortMsgLabel
      //
      this.prt_Gen_AbortMsgLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_AbortMsgLabel.Location = new System.Drawing.Point(168, 94);
      this.prt_Gen_AbortMsgLabel.Name = "prt_Gen_AbortMsgLabel";
      this.prt_Gen_AbortMsgLabel.Size = new System.Drawing.Size(103, 27);
      this.prt_Gen_AbortMsgLabel.TabIndex = 9;
      this.prt_Gen_AbortMsgLabel.Text = "&Abort Message:";
      this.prt_Gen_AbortMsgLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Gen_JobName
      //
      this.prt_Gen_JobName.Location = new System.Drawing.Point(280, 56);
      this.prt_Gen_JobName.Name = "prt_Gen_JobName";
      this.prt_Gen_JobName.Size = new System.Drawing.Size(234, 20);
      this.prt_Gen_JobName.TabIndex = 8;
      //
      //prt_Gen_PrintJobLabel
      //
      this.prt_Gen_PrintJobLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_PrintJobLabel.Location = new System.Drawing.Point(168, 56);
      this.prt_Gen_PrintJobLabel.Name = "prt_Gen_PrintJobLabel";
      this.prt_Gen_PrintJobLabel.Size = new System.Drawing.Size(103, 28);
      this.prt_Gen_PrintJobLabel.TabIndex = 7;
      this.prt_Gen_PrintJobLabel.Text = "Print &Job Name:";
      this.prt_Gen_PrintJobLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Gen_Printer
      //
      this.prt_Gen_Printer.Location = new System.Drawing.Point(280, 19);
      this.prt_Gen_Printer.Name = "prt_Gen_Printer";
      this.prt_Gen_Printer.Size = new System.Drawing.Size(234, 20);
      this.prt_Gen_Printer.TabIndex = 6;
      //
      //prt_Gen_PrinterNameLabel
      //
      this.prt_Gen_PrinterNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_PrinterNameLabel.Location = new System.Drawing.Point(168, 19);
      this.prt_Gen_PrinterNameLabel.Name = "prt_Gen_PrinterNameLabel";
      this.prt_Gen_PrinterNameLabel.Size = new System.Drawing.Size(103, 27);
      this.prt_Gen_PrinterNameLabel.TabIndex = 5;
      this.prt_Gen_PrinterNameLabel.Text = "&Printer Name:";
      this.prt_Gen_PrinterNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Gen_Preview
      //
      this.prt_Gen_Preview.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Gen_Preview.Location = new System.Drawing.Point(9, 19);
      this.prt_Gen_Preview.Name = "prt_Gen_Preview";
      this.prt_Gen_Preview.Size = new System.Drawing.Size(132, 27);
      this.prt_Gen_Preview.TabIndex = 0;
      this.prt_Gen_Preview.Text = "Pre&view Only";
      //
      //prt_TabSmartPrint
      //
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_DeleteButton);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_RulesCollection);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_ScaleInterval);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_ScaleEndFactor);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_ScaleStartFactor);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_IntervalLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_EndLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_StartLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_ResetOption);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_ResetLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_OptionsLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_AddButton);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_Rule);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_RuleLabel);
      this.prt_TabSmartPrint.Controls.Add(this.prt_SmartPrint_UseSmartPrint);
      this.prt_TabSmartPrint.Location = new System.Drawing.Point(4, 22);
      this.prt_TabSmartPrint.Name = "prt_TabSmartPrint";
      this.prt_TabSmartPrint.Size = new System.Drawing.Size(526, 310);
      this.prt_TabSmartPrint.TabIndex = 4;
      this.prt_TabSmartPrint.Text = "SmartPrint";
      this.prt_TabSmartPrint.UseVisualStyleBackColor = true;
      //
      //prt_SmartPrint_DeleteButton
      //
      this.prt_SmartPrint_DeleteButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_DeleteButton.Location = new System.Drawing.Point(427, 36);
      this.prt_SmartPrint_DeleteButton.Name = "prt_SmartPrint_DeleteButton";
      this.prt_SmartPrint_DeleteButton.Size = new System.Drawing.Size(86, 27);
      this.prt_SmartPrint_DeleteButton.TabIndex = 2;
      this.prt_SmartPrint_DeleteButton.Text = "&Delete";
      //
      //prt_SmartPrint_RulesCollection
      //
      this.prt_SmartPrint_RulesCollection.HorizontalScrollbar = true;
      this.prt_SmartPrint_RulesCollection.Location = new System.Drawing.Point(16, 35);
      this.prt_SmartPrint_RulesCollection.Name = "prt_SmartPrint_RulesCollection";
      this.prt_SmartPrint_RulesCollection.Size = new System.Drawing.Size(400, 95);
      this.prt_SmartPrint_RulesCollection.TabIndex = 1;
      //
      //prt_SmartPrint_ScaleInterval
      //
      this.prt_SmartPrint_ScaleInterval.Location = new System.Drawing.Point(404, 253);
      this.prt_SmartPrint_ScaleInterval.Name = "prt_SmartPrint_ScaleInterval";
      this.prt_SmartPrint_ScaleInterval.Size = new System.Drawing.Size(75, 20);
      this.prt_SmartPrint_ScaleInterval.TabIndex = 14;
      //
      //prt_SmartPrint_ScaleEndFactor
      //
      this.prt_SmartPrint_ScaleEndFactor.Location = new System.Drawing.Point(167, 268);
      this.prt_SmartPrint_ScaleEndFactor.Name = "prt_SmartPrint_ScaleEndFactor";
      this.prt_SmartPrint_ScaleEndFactor.Size = new System.Drawing.Size(76, 20);
      this.prt_SmartPrint_ScaleEndFactor.TabIndex = 12;
      //
      //prt_SmartPrint_ScaleStartFactor
      //
      this.prt_SmartPrint_ScaleStartFactor.Location = new System.Drawing.Point(167, 236);
      this.prt_SmartPrint_ScaleStartFactor.Name = "prt_SmartPrint_ScaleStartFactor";
      this.prt_SmartPrint_ScaleStartFactor.Size = new System.Drawing.Size(76, 20);
      this.prt_SmartPrint_ScaleStartFactor.TabIndex = 10;
      //
      //prt_SmartPrint_IntervalLabel
      //
      this.prt_SmartPrint_IntervalLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_IntervalLabel.Location = new System.Drawing.Point(264, 253);
      this.prt_SmartPrint_IntervalLabel.Name = "prt_SmartPrint_IntervalLabel";
      this.prt_SmartPrint_IntervalLabel.Size = new System.Drawing.Size(131, 22);
      this.prt_SmartPrint_IntervalLabel.TabIndex = 13;
      this.prt_SmartPrint_IntervalLabel.Text = "&Interval:";
      this.prt_SmartPrint_IntervalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_SmartPrint_EndLabel
      //
      this.prt_SmartPrint_EndLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_EndLabel.Location = new System.Drawing.Point(16, 268);
      this.prt_SmartPrint_EndLabel.Name = "prt_SmartPrint_EndLabel";
      this.prt_SmartPrint_EndLabel.Size = new System.Drawing.Size(130, 23);
      this.prt_SmartPrint_EndLabel.TabIndex = 11;
      this.prt_SmartPrint_EndLabel.Text = "&End Factor:";
      this.prt_SmartPrint_EndLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_SmartPrint_StartLabel
      //
      this.prt_SmartPrint_StartLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_StartLabel.Location = new System.Drawing.Point(16, 236);
      this.prt_SmartPrint_StartLabel.Name = "prt_SmartPrint_StartLabel";
      this.prt_SmartPrint_StartLabel.Size = new System.Drawing.Size(130, 23);
      this.prt_SmartPrint_StartLabel.TabIndex = 9;
      this.prt_SmartPrint_StartLabel.Text = "&Start Factor:";
      this.prt_SmartPrint_StartLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_SmartPrint_ResetOption
      //
      this.prt_SmartPrint_ResetOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_SmartPrint_ResetOption.ItemHeight = 13;
      this.prt_SmartPrint_ResetOption.Items.AddRange(new object[] {
			"None",
			"Current Rule",
			"All Rules"
		});
      this.prt_SmartPrint_ResetOption.Location = new System.Drawing.Point(167, 204);
      this.prt_SmartPrint_ResetOption.Name = "prt_SmartPrint_ResetOption";
      this.prt_SmartPrint_ResetOption.Size = new System.Drawing.Size(247, 21);
      this.prt_SmartPrint_ResetOption.TabIndex = 8;
      //
      //prt_SmartPrint_ResetLabel
      //
      this.prt_SmartPrint_ResetLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_ResetLabel.Location = new System.Drawing.Point(16, 204);
      this.prt_SmartPrint_ResetLabel.Name = "prt_SmartPrint_ResetLabel";
      this.prt_SmartPrint_ResetLabel.Size = new System.Drawing.Size(148, 21);
      this.prt_SmartPrint_ResetLabel.TabIndex = 7;
      this.prt_SmartPrint_ResetLabel.Text = "&Reset Option:";
      this.prt_SmartPrint_ResetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_SmartPrint_OptionsLabel
      //
      this.prt_SmartPrint_OptionsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_OptionsLabel.Location = new System.Drawing.Point(16, 175);
      this.prt_SmartPrint_OptionsLabel.Name = "prt_SmartPrint_OptionsLabel";
      this.prt_SmartPrint_OptionsLabel.Size = new System.Drawing.Size(506, 21);
      this.prt_SmartPrint_OptionsLabel.TabIndex = 6;
      this.prt_SmartPrint_OptionsLabel.Text = "Rule &Options";
      //
      //prt_SmartPrint_AddButton
      //
      this.prt_SmartPrint_AddButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_AddButton.Location = new System.Drawing.Point(427, 136);
      this.prt_SmartPrint_AddButton.Name = "prt_SmartPrint_AddButton";
      this.prt_SmartPrint_AddButton.Size = new System.Drawing.Size(88, 29);
      this.prt_SmartPrint_AddButton.TabIndex = 5;
      this.prt_SmartPrint_AddButton.Text = "&Add";
      //
      //prt_SmartPrint_Rule
      //
      this.prt_SmartPrint_Rule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_SmartPrint_Rule.ItemHeight = 13;
      this.prt_SmartPrint_Rule.Items.AddRange(new object[] {
			"Scale Rule",
			"Landscape Rule",
			"Best Fit Column Rule",
			"Smart Paper Rule"
		});
      this.prt_SmartPrint_Rule.Location = new System.Drawing.Point(168, 139);
      this.prt_SmartPrint_Rule.Name = "prt_SmartPrint_Rule";
      this.prt_SmartPrint_Rule.Size = new System.Drawing.Size(247, 21);
      this.prt_SmartPrint_Rule.TabIndex = 4;
      //
      //prt_SmartPrint_RuleLabel
      //
      this.prt_SmartPrint_RuleLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_RuleLabel.Location = new System.Drawing.Point(16, 138);
      this.prt_SmartPrint_RuleLabel.Name = "prt_SmartPrint_RuleLabel";
      this.prt_SmartPrint_RuleLabel.Size = new System.Drawing.Size(148, 22);
      this.prt_SmartPrint_RuleLabel.TabIndex = 3;
      this.prt_SmartPrint_RuleLabel.Text = "Smart &Print Rule:";
      this.prt_SmartPrint_RuleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_SmartPrint_UseSmartPrint
      //
      this.prt_SmartPrint_UseSmartPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_SmartPrint_UseSmartPrint.Location = new System.Drawing.Point(9, 9);
      this.prt_SmartPrint_UseSmartPrint.Name = "prt_SmartPrint_UseSmartPrint";
      this.prt_SmartPrint_UseSmartPrint.Size = new System.Drawing.Size(281, 19);
      this.prt_SmartPrint_UseSmartPrint.TabIndex = 0;
      this.prt_SmartPrint_UseSmartPrint.Text = "&Use SmartPrint";
      //
      //prt_TabOutput
      //
      this.prt_TabOutput.Controls.Add(this.prt_Out_Orientation);
      this.prt_TabOutput.Controls.Add(this.prt_Out_OrientationLabel);
      this.prt_TabOutput.Controls.Add(this.prt_Out_PrintType);
      this.prt_TabOutput.Controls.Add(this.prt_Out_TypeLabel);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowPrintDialog);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowShadows);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowGrid);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowRowHeaders);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowColumnHeaders);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowColor);
      this.prt_TabOutput.Controls.Add(this.prt_Out_ShowBorder);
      this.prt_TabOutput.Location = new System.Drawing.Point(4, 22);
      this.prt_TabOutput.Name = "prt_TabOutput";
      this.prt_TabOutput.Size = new System.Drawing.Size(526, 310);
      this.prt_TabOutput.TabIndex = 1;
      this.prt_TabOutput.Text = "Output";
      this.prt_TabOutput.UseVisualStyleBackColor = true;
      //
      //prt_Out_Orientation
      //
      this.prt_Out_Orientation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_Out_Orientation.ItemHeight = 13;
      this.prt_Out_Orientation.Items.AddRange(new object[] {
			"Auto",
			"Portrait",
			"Landscape"
		});
      this.prt_Out_Orientation.Location = new System.Drawing.Point(243, 131);
      this.prt_Out_Orientation.Name = "prt_Out_Orientation";
      this.prt_Out_Orientation.Size = new System.Drawing.Size(234, 21);
      this.prt_Out_Orientation.TabIndex = 10;
      //
      //prt_Out_OrientationLabel
      //
      this.prt_Out_OrientationLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_OrientationLabel.Location = new System.Drawing.Point(243, 94);
      this.prt_Out_OrientationLabel.Name = "prt_Out_OrientationLabel";
      this.prt_Out_OrientationLabel.Size = new System.Drawing.Size(234, 27);
      this.prt_Out_OrientationLabel.TabIndex = 9;
      this.prt_Out_OrientationLabel.Text = "&Orientation:";
      //
      //prt_Out_PrintType
      //
      this.prt_Out_PrintType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_Out_PrintType.ItemHeight = 13;
      this.prt_Out_PrintType.Items.AddRange(new object[] {
			"All",
			"Cell Range",
			"Current Page",
			"Page Range"
		});
      this.prt_Out_PrintType.Location = new System.Drawing.Point(243, 56);
      this.prt_Out_PrintType.Name = "prt_Out_PrintType";
      this.prt_Out_PrintType.Size = new System.Drawing.Size(234, 21);
      this.prt_Out_PrintType.TabIndex = 8;
      //
      //prt_Out_TypeLabel
      //
      this.prt_Out_TypeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_TypeLabel.Location = new System.Drawing.Point(243, 19);
      this.prt_Out_TypeLabel.Name = "prt_Out_TypeLabel";
      this.prt_Out_TypeLabel.Size = new System.Drawing.Size(234, 27);
      this.prt_Out_TypeLabel.TabIndex = 7;
      this.prt_Out_TypeLabel.Text = "Output &Type:";
      //
      //prt_Out_ShowPrintDialog
      //
      this.prt_Out_ShowPrintDialog.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowPrintDialog.Location = new System.Drawing.Point(37, 234);
      this.prt_Out_ShowPrintDialog.Name = "prt_Out_ShowPrintDialog";
      this.prt_Out_ShowPrintDialog.Size = new System.Drawing.Size(197, 27);
      this.prt_Out_ShowPrintDialog.TabIndex = 6;
      this.prt_Out_ShowPrintDialog.Text = "Show &Print Dialog";
      //
      //prt_Out_ShowShadows
      //
      this.prt_Out_ShowShadows.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowShadows.Location = new System.Drawing.Point(37, 159);
      this.prt_Out_ShowShadows.Name = "prt_Out_ShowShadows";
      this.prt_Out_ShowShadows.Size = new System.Drawing.Size(197, 27);
      this.prt_Out_ShowShadows.TabIndex = 5;
      this.prt_Out_ShowShadows.Text = "Show &Shadows";
      //
      //prt_Out_ShowGrid
      //
      this.prt_Out_ShowGrid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowGrid.Location = new System.Drawing.Point(37, 131);
      this.prt_Out_ShowGrid.Name = "prt_Out_ShowGrid";
      this.prt_Out_ShowGrid.Size = new System.Drawing.Size(197, 28);
      this.prt_Out_ShowGrid.TabIndex = 4;
      this.prt_Out_ShowGrid.Text = "Show &Grid";
      //
      //prt_Out_ShowRowHeaders
      //
      this.prt_Out_ShowRowHeaders.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowRowHeaders.Location = new System.Drawing.Point(37, 103);
      this.prt_Out_ShowRowHeaders.Name = "prt_Out_ShowRowHeaders";
      this.prt_Out_ShowRowHeaders.Size = new System.Drawing.Size(197, 28);
      this.prt_Out_ShowRowHeaders.TabIndex = 3;
      this.prt_Out_ShowRowHeaders.Text = "Show &Row Headers";
      //
      //prt_Out_ShowColumnHeaders
      //
      this.prt_Out_ShowColumnHeaders.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowColumnHeaders.Location = new System.Drawing.Point(37, 75);
      this.prt_Out_ShowColumnHeaders.Name = "prt_Out_ShowColumnHeaders";
      this.prt_Out_ShowColumnHeaders.Size = new System.Drawing.Size(197, 28);
      this.prt_Out_ShowColumnHeaders.TabIndex = 2;
      this.prt_Out_ShowColumnHeaders.Text = "Show &Column Headers";
      //
      //prt_Out_ShowColor
      //
      this.prt_Out_ShowColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowColor.Location = new System.Drawing.Point(37, 46);
      this.prt_Out_ShowColor.Name = "prt_Out_ShowColor";
      this.prt_Out_ShowColor.Size = new System.Drawing.Size(197, 29);
      this.prt_Out_ShowColor.TabIndex = 1;
      this.prt_Out_ShowColor.Text = "Show Co&lor";
      //
      //prt_Out_ShowBorder
      //
      this.prt_Out_ShowBorder.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Out_ShowBorder.Location = new System.Drawing.Point(37, 19);
      this.prt_Out_ShowBorder.Name = "prt_Out_ShowBorder";
      this.prt_Out_ShowBorder.Size = new System.Drawing.Size(197, 27);
      this.prt_Out_ShowBorder.TabIndex = 0;
      this.prt_Out_ShowBorder.Text = "Show &Border";
      //
      //prt_TabHeadFoot
      //
      this.prt_TabHeadFoot.Controls.Add(this.prt_HeadFoot_FooterText);
      this.prt_TabHeadFoot.Controls.Add(this.Panel1);
      this.prt_TabHeadFoot.Controls.Add(this.prt_HeadFoot_AddButton);
      this.prt_TabHeadFoot.Controls.Add(this.prt_HeadFoot_ControlChars);
      this.prt_TabHeadFoot.Controls.Add(this.prt_HeadFoot_ControlCharsLabel);
      this.prt_TabHeadFoot.Controls.Add(this.prt_HeadFoot_HeaderText);
      this.prt_TabHeadFoot.Location = new System.Drawing.Point(4, 22);
      this.prt_TabHeadFoot.Name = "prt_TabHeadFoot";
      this.prt_TabHeadFoot.Size = new System.Drawing.Size(526, 310);
      this.prt_TabHeadFoot.TabIndex = 3;
      this.prt_TabHeadFoot.Text = "Print Title";
      this.prt_TabHeadFoot.UseVisualStyleBackColor = true;
      //
      //prt_HeadFoot_FooterText
      //
      this.prt_HeadFoot_FooterText.Location = new System.Drawing.Point(9, 46);
      this.prt_HeadFoot_FooterText.Multiline = true;
      this.prt_HeadFoot_FooterText.Name = "prt_HeadFoot_FooterText";
      this.prt_HeadFoot_FooterText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.prt_HeadFoot_FooterText.Size = new System.Drawing.Size(505, 207);
      this.prt_HeadFoot_FooterText.TabIndex = 3;
      this.prt_HeadFoot_FooterText.Visible = false;
      //
      //Panel1
      //
      this.Panel1.Controls.Add(this.prt_HeadFoot_Footer);
      this.Panel1.Controls.Add(this.prt_HeadFoot_Header);
      this.Panel1.Location = new System.Drawing.Point(9, 0);
      this.Panel1.Name = "Panel1";
      this.Panel1.Size = new System.Drawing.Size(505, 46);
      this.Panel1.TabIndex = 0;
      //
      //prt_HeadFoot_Footer
      //
      this.prt_HeadFoot_Footer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_HeadFoot_Footer.Location = new System.Drawing.Point(280, 14);
      this.prt_HeadFoot_Footer.Name = "prt_HeadFoot_Footer";
      this.prt_HeadFoot_Footer.Size = new System.Drawing.Size(206, 19);
      this.prt_HeadFoot_Footer.TabIndex = 2;
      this.prt_HeadFoot_Footer.Text = "&Footer";
      //
      //prt_HeadFoot_Header
      //
      this.prt_HeadFoot_Header.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_HeadFoot_Header.Location = new System.Drawing.Point(9, 14);
      this.prt_HeadFoot_Header.Name = "prt_HeadFoot_Header";
      this.prt_HeadFoot_Header.Size = new System.Drawing.Size(207, 19);
      this.prt_HeadFoot_Header.TabIndex = 1;
      this.prt_HeadFoot_Header.Text = "&Header";
      //
      //prt_HeadFoot_AddButton
      //
      this.prt_HeadFoot_AddButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_HeadFoot_AddButton.Location = new System.Drawing.Point(430, 261);
      this.prt_HeadFoot_AddButton.Name = "prt_HeadFoot_AddButton";
      this.prt_HeadFoot_AddButton.Size = new System.Drawing.Size(84, 29);
      this.prt_HeadFoot_AddButton.TabIndex = 7;
      this.prt_HeadFoot_AddButton.Text = "&Add";
      //
      //prt_HeadFoot_ControlChars
      //
      this.prt_HeadFoot_ControlChars.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_HeadFoot_ControlChars.ItemHeight = 13;
      this.prt_HeadFoot_ControlChars.Items.AddRange(new object[] {
			"/n  New line  ",
			"/l  Left aligns text  ",
			"/r  Right aligns text  ",
			"/c  Centers text  ",
			"/p  Inserts page number  ",
			"/pc  Inserts page count  ",
			"//  Prints a slash character (/)  ",
			"/fn  Font name, must be enclosed in quotes  ",
			"/fz  Font size in points, must be enclosed in quotes  ",
			"/fb0  Font bold off  ",
			"/fb1  Font bold on  ",
			"/fi0  Font italics off  ",
			"/fi1  Font italics on  ",
			"/fu0  Font underline off  ",
			"/fu1  Font underline on  ",
			"/fk0  Font strikethrough off  ",
			"/fk1  Font strikethrough on"
		});
      this.prt_HeadFoot_ControlChars.Location = new System.Drawing.Point(150, 261);
      this.prt_HeadFoot_ControlChars.Name = "prt_HeadFoot_ControlChars";
      this.prt_HeadFoot_ControlChars.Size = new System.Drawing.Size(262, 21);
      this.prt_HeadFoot_ControlChars.TabIndex = 6;
      //
      //prt_HeadFoot_ControlCharsLabel
      //
      this.prt_HeadFoot_ControlCharsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_HeadFoot_ControlCharsLabel.Location = new System.Drawing.Point(9, 261);
      this.prt_HeadFoot_ControlCharsLabel.Name = "prt_HeadFoot_ControlCharsLabel";
      this.prt_HeadFoot_ControlCharsLabel.Size = new System.Drawing.Size(142, 29);
      this.prt_HeadFoot_ControlCharsLabel.TabIndex = 5;
      this.prt_HeadFoot_ControlCharsLabel.Text = "&Control Characters:";
      //
      //prt_HeadFoot_HeaderText
      //
      this.prt_HeadFoot_HeaderText.Location = new System.Drawing.Point(9, 46);
      this.prt_HeadFoot_HeaderText.Multiline = true;
      this.prt_HeadFoot_HeaderText.Name = "prt_HeadFoot_HeaderText";
      this.prt_HeadFoot_HeaderText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.prt_HeadFoot_HeaderText.Size = new System.Drawing.Size(505, 207);
      this.prt_HeadFoot_HeaderText.TabIndex = 4;
      this.prt_HeadFoot_HeaderText.Visible = false;
      //
      //prt_TabPagination
      //
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageOrder);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_RowEnd);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_RowEndLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_RowStart);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_RowStartLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_ColumnEnd);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_ColEndLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_ColumnStart);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_ColStartLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageEnd);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageEndLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageStart);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageStartLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageOrderLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_FirstPageNumber);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_FirstPageLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_RowLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_ColumnLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_PageLabel);
      this.prt_TabPagination.Controls.Add(this.prt_Pag_SettingsLabel);
      this.prt_TabPagination.Location = new System.Drawing.Point(4, 22);
      this.prt_TabPagination.Name = "prt_TabPagination";
      this.prt_TabPagination.Size = new System.Drawing.Size(526, 310);
      this.prt_TabPagination.TabIndex = 2;
      this.prt_TabPagination.Text = "Pagination";
      this.prt_TabPagination.UseVisualStyleBackColor = true;
      //
      //prt_Pag_PageOrder
      //
      this.prt_Pag_PageOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.prt_Pag_PageOrder.ItemHeight = 13;
      this.prt_Pag_PageOrder.Items.AddRange(new object[] {
			"Auto",
			"Down Then Over",
			"Over Then Down"
		});
      this.prt_Pag_PageOrder.Location = new System.Drawing.Point(366, 44);
      this.prt_Pag_PageOrder.Name = "prt_Pag_PageOrder";
      this.prt_Pag_PageOrder.Size = new System.Drawing.Size(147, 21);
      this.prt_Pag_PageOrder.TabIndex = 4;
      //
      //prt_Pag_RowEnd
      //
      this.prt_Pag_RowEnd.Location = new System.Drawing.Point(421, 271);
      this.prt_Pag_RowEnd.Name = "prt_Pag_RowEnd";
      this.prt_Pag_RowEnd.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_RowEnd.TabIndex = 19;
      //
      //prt_Pag_RowEndLabel
      //
      this.prt_Pag_RowEndLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_RowEndLabel.Location = new System.Drawing.Point(271, 271);
      this.prt_Pag_RowEndLabel.Name = "prt_Pag_RowEndLabel";
      this.prt_Pag_RowEndLabel.Size = new System.Drawing.Size(123, 25);
      this.prt_Pag_RowEndLabel.TabIndex = 18;
      this.prt_Pag_RowEndLabel.Text = "Row En&d:";
      this.prt_Pag_RowEndLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_RowStart
      //
      this.prt_Pag_RowStart.Location = new System.Drawing.Point(154, 271);
      this.prt_Pag_RowStart.Name = "prt_Pag_RowStart";
      this.prt_Pag_RowStart.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_RowStart.TabIndex = 17;
      //
      //prt_Pag_RowStartLabel
      //
      this.prt_Pag_RowStartLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_RowStartLabel.Location = new System.Drawing.Point(17, 271);
      this.prt_Pag_RowStartLabel.Name = "prt_Pag_RowStartLabel";
      this.prt_Pag_RowStartLabel.Size = new System.Drawing.Size(124, 25);
      this.prt_Pag_RowStartLabel.TabIndex = 16;
      this.prt_Pag_RowStartLabel.Text = "&Row Start:";
      this.prt_Pag_RowStartLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_ColumnEnd
      //
      this.prt_Pag_ColumnEnd.Location = new System.Drawing.Point(421, 196);
      this.prt_Pag_ColumnEnd.Name = "prt_Pag_ColumnEnd";
      this.prt_Pag_ColumnEnd.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_ColumnEnd.TabIndex = 14;
      //
      //prt_Pag_ColEndLabel
      //
      this.prt_Pag_ColEndLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_ColEndLabel.Location = new System.Drawing.Point(271, 196);
      this.prt_Pag_ColEndLabel.Name = "prt_Pag_ColEndLabel";
      this.prt_Pag_ColEndLabel.Size = new System.Drawing.Size(123, 26);
      this.prt_Pag_ColEndLabel.TabIndex = 13;
      this.prt_Pag_ColEndLabel.Text = "Column E&nd:";
      this.prt_Pag_ColEndLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_ColumnStart
      //
      this.prt_Pag_ColumnStart.Location = new System.Drawing.Point(154, 196);
      this.prt_Pag_ColumnStart.Name = "prt_Pag_ColumnStart";
      this.prt_Pag_ColumnStart.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_ColumnStart.TabIndex = 12;
      //
      //prt_Pag_ColStartLabel
      //
      this.prt_Pag_ColStartLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_ColStartLabel.Location = new System.Drawing.Point(17, 196);
      this.prt_Pag_ColStartLabel.Name = "prt_Pag_ColStartLabel";
      this.prt_Pag_ColStartLabel.Size = new System.Drawing.Size(124, 26);
      this.prt_Pag_ColStartLabel.TabIndex = 11;
      this.prt_Pag_ColStartLabel.Text = "&Column Start:";
      this.prt_Pag_ColStartLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_PageEnd
      //
      this.prt_Pag_PageEnd.Location = new System.Drawing.Point(421, 121);
      this.prt_Pag_PageEnd.Name = "prt_Pag_PageEnd";
      this.prt_Pag_PageEnd.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_PageEnd.TabIndex = 9;
      //
      //prt_Pag_PageEndLabel
      //
      this.prt_Pag_PageEndLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_PageEndLabel.Location = new System.Drawing.Point(271, 121);
      this.prt_Pag_PageEndLabel.Name = "prt_Pag_PageEndLabel";
      this.prt_Pag_PageEndLabel.Size = new System.Drawing.Size(123, 26);
      this.prt_Pag_PageEndLabel.TabIndex = 8;
      this.prt_Pag_PageEndLabel.Text = "Page &End:";
      this.prt_Pag_PageEndLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_PageStart
      //
      this.prt_Pag_PageStart.Location = new System.Drawing.Point(154, 121);
      this.prt_Pag_PageStart.Name = "prt_Pag_PageStart";
      this.prt_Pag_PageStart.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_PageStart.TabIndex = 7;
      //
      //prt_Pag_PageStartLabel
      //
      this.prt_Pag_PageStartLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_PageStartLabel.Location = new System.Drawing.Point(17, 121);
      this.prt_Pag_PageStartLabel.Name = "prt_Pag_PageStartLabel";
      this.prt_Pag_PageStartLabel.Size = new System.Drawing.Size(124, 26);
      this.prt_Pag_PageStartLabel.TabIndex = 6;
      this.prt_Pag_PageStartLabel.Text = "Page &Start:";
      this.prt_Pag_PageStartLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_PageOrderLabel
      //
      this.prt_Pag_PageOrderLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_PageOrderLabel.Location = new System.Drawing.Point(271, 44);
      this.prt_Pag_PageOrderLabel.Name = "prt_Pag_PageOrderLabel";
      this.prt_Pag_PageOrderLabel.Size = new System.Drawing.Size(91, 25);
      this.prt_Pag_PageOrderLabel.TabIndex = 3;
      this.prt_Pag_PageOrderLabel.Text = "Page &Order:";
      this.prt_Pag_PageOrderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_FirstPageNumber
      //
      this.prt_Pag_FirstPageNumber.Location = new System.Drawing.Point(154, 44);
      this.prt_Pag_FirstPageNumber.Name = "prt_Pag_FirstPageNumber";
      this.prt_Pag_FirstPageNumber.Size = new System.Drawing.Size(84, 20);
      this.prt_Pag_FirstPageNumber.TabIndex = 2;
      //
      //prt_Pag_FirstPageLabel
      //
      this.prt_Pag_FirstPageLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_FirstPageLabel.Location = new System.Drawing.Point(17, 44);
      this.prt_Pag_FirstPageLabel.Name = "prt_Pag_FirstPageLabel";
      this.prt_Pag_FirstPageLabel.Size = new System.Drawing.Size(124, 25);
      this.prt_Pag_FirstPageLabel.TabIndex = 1;
      this.prt_Pag_FirstPageLabel.Text = "&First Page Number:";
      this.prt_Pag_FirstPageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Pag_RowLabel
      //
      this.prt_Pag_RowLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_RowLabel.Location = new System.Drawing.Point(9, 243);
      this.prt_Pag_RowLabel.Name = "prt_Pag_RowLabel";
      this.prt_Pag_RowLabel.Size = new System.Drawing.Size(505, 18);
      this.prt_Pag_RowLabel.TabIndex = 15;
      this.prt_Pag_RowLabel.Text = "Row";
      //
      //prt_Pag_ColumnLabel
      //
      this.prt_Pag_ColumnLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_ColumnLabel.Location = new System.Drawing.Point(9, 169);
      this.prt_Pag_ColumnLabel.Name = "prt_Pag_ColumnLabel";
      this.prt_Pag_ColumnLabel.Size = new System.Drawing.Size(505, 17);
      this.prt_Pag_ColumnLabel.TabIndex = 10;
      this.prt_Pag_ColumnLabel.Text = "Column";
      //
      //prt_Pag_PageLabel
      //
      this.prt_Pag_PageLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_PageLabel.Location = new System.Drawing.Point(9, 89);
      this.prt_Pag_PageLabel.Name = "prt_Pag_PageLabel";
      this.prt_Pag_PageLabel.Size = new System.Drawing.Size(505, 19);
      this.prt_Pag_PageLabel.TabIndex = 5;
      this.prt_Pag_PageLabel.Text = "Page";
      //
      //prt_Pag_SettingsLabel
      //
      this.prt_Pag_SettingsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Pag_SettingsLabel.Location = new System.Drawing.Point(9, 19);
      this.prt_Pag_SettingsLabel.Name = "prt_Pag_SettingsLabel";
      this.prt_Pag_SettingsLabel.Size = new System.Drawing.Size(505, 18);
      this.prt_Pag_SettingsLabel.TabIndex = 0;
      this.prt_Pag_SettingsLabel.Text = "Settings";
      //
      //prt_TabMargins
      //
      this.prt_TabMargins.Controls.Add(this.prt_Margin_SizingNote);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_RegionsLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Footer);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_FooterLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Header);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_HeaderLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Bottom);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_BottomLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Right);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_RightLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Left);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_LeftLabel);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_Top);
      this.prt_TabMargins.Controls.Add(this.prt_Margin_TopLabel);
      this.prt_TabMargins.Location = new System.Drawing.Point(4, 22);
      this.prt_TabMargins.Name = "prt_TabMargins";
      this.prt_TabMargins.Size = new System.Drawing.Size(526, 310);
      this.prt_TabMargins.TabIndex = 5;
      this.prt_TabMargins.Text = "Margins";
      this.prt_TabMargins.UseVisualStyleBackColor = true;
      //
      //prt_Margin_SizingNote
      //
      this.prt_Margin_SizingNote.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_SizingNote.Location = new System.Drawing.Point(263, 22);
      this.prt_Margin_SizingNote.Name = "prt_Margin_SizingNote";
      this.prt_Margin_SizingNote.Size = new System.Drawing.Size(242, 64);
      this.prt_Margin_SizingNote.TabIndex = 13;
      this.prt_Margin_SizingNote.Text = "All values on this page are specified in inches or fractions of inches.";
      //
      //prt_Margin_RegionsLabel
      //
      this.prt_Margin_RegionsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_RegionsLabel.Location = new System.Drawing.Point(17, 178);
      this.prt_Margin_RegionsLabel.Name = "prt_Margin_RegionsLabel";
      this.prt_Margin_RegionsLabel.Size = new System.Drawing.Size(496, 27);
      this.prt_Margin_RegionsLabel.TabIndex = 8;
      this.prt_Margin_RegionsLabel.Text = "Special Regions";
      //
      //prt_Margin_Footer
      //
      this.prt_Margin_Footer.Location = new System.Drawing.Point(129, 261);
      this.prt_Margin_Footer.Name = "prt_Margin_Footer";
      this.prt_Margin_Footer.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Footer.TabIndex = 12;
      //
      //prt_Margin_FooterLabel
      //
      this.prt_Margin_FooterLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_FooterLabel.Location = new System.Drawing.Point(17, 261);
      this.prt_Margin_FooterLabel.Name = "prt_Margin_FooterLabel";
      this.prt_Margin_FooterLabel.Size = new System.Drawing.Size(107, 23);
      this.prt_Margin_FooterLabel.TabIndex = 11;
      this.prt_Margin_FooterLabel.Text = "&Footer:";
      this.prt_Margin_FooterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Margin_Header
      //
      this.prt_Margin_Header.Location = new System.Drawing.Point(129, 224);
      this.prt_Margin_Header.Name = "prt_Margin_Header";
      this.prt_Margin_Header.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Header.TabIndex = 10;
      //
      //prt_Margin_HeaderLabel
      //
      this.prt_Margin_HeaderLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_HeaderLabel.Location = new System.Drawing.Point(17, 224);
      this.prt_Margin_HeaderLabel.Name = "prt_Margin_HeaderLabel";
      this.prt_Margin_HeaderLabel.Size = new System.Drawing.Size(107, 22);
      this.prt_Margin_HeaderLabel.TabIndex = 9;
      this.prt_Margin_HeaderLabel.Text = "&Header:";
      this.prt_Margin_HeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Margin_Bottom
      //
      this.prt_Margin_Bottom.Location = new System.Drawing.Point(129, 131);
      this.prt_Margin_Bottom.Name = "prt_Margin_Bottom";
      this.prt_Margin_Bottom.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Bottom.TabIndex = 7;
      //
      //prt_Margin_BottomLabel
      //
      this.prt_Margin_BottomLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_BottomLabel.Location = new System.Drawing.Point(17, 131);
      this.prt_Margin_BottomLabel.Name = "prt_Margin_BottomLabel";
      this.prt_Margin_BottomLabel.Size = new System.Drawing.Size(107, 22);
      this.prt_Margin_BottomLabel.TabIndex = 6;
      this.prt_Margin_BottomLabel.Text = "&Bottom:";
      this.prt_Margin_BottomLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Margin_Right
      //
      this.prt_Margin_Right.Location = new System.Drawing.Point(129, 94);
      this.prt_Margin_Right.Name = "prt_Margin_Right";
      this.prt_Margin_Right.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Right.TabIndex = 5;
      //
      //prt_Margin_RightLabel
      //
      this.prt_Margin_RightLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_RightLabel.Location = new System.Drawing.Point(17, 94);
      this.prt_Margin_RightLabel.Name = "prt_Margin_RightLabel";
      this.prt_Margin_RightLabel.Size = new System.Drawing.Size(107, 21);
      this.prt_Margin_RightLabel.TabIndex = 4;
      this.prt_Margin_RightLabel.Text = "&Right:";
      this.prt_Margin_RightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Margin_Left
      //
      this.prt_Margin_Left.Location = new System.Drawing.Point(129, 56);
      this.prt_Margin_Left.Name = "prt_Margin_Left";
      this.prt_Margin_Left.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Left.TabIndex = 3;
      //
      //prt_Margin_LeftLabel
      //
      this.prt_Margin_LeftLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_LeftLabel.Location = new System.Drawing.Point(17, 56);
      this.prt_Margin_LeftLabel.Name = "prt_Margin_LeftLabel";
      this.prt_Margin_LeftLabel.Size = new System.Drawing.Size(107, 23);
      this.prt_Margin_LeftLabel.TabIndex = 2;
      this.prt_Margin_LeftLabel.Text = "&Left:";
      this.prt_Margin_LeftLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_Margin_Top
      //
      this.prt_Margin_Top.Location = new System.Drawing.Point(129, 19);
      this.prt_Margin_Top.Name = "prt_Margin_Top";
      this.prt_Margin_Top.Size = new System.Drawing.Size(108, 20);
      this.prt_Margin_Top.TabIndex = 1;
      //
      //prt_Margin_TopLabel
      //
      this.prt_Margin_TopLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_Margin_TopLabel.Location = new System.Drawing.Point(17, 19);
      this.prt_Margin_TopLabel.Name = "prt_Margin_TopLabel";
      this.prt_Margin_TopLabel.Size = new System.Drawing.Size(107, 22);
      this.prt_Margin_TopLabel.TabIndex = 0;
      this.prt_Margin_TopLabel.Text = "&Top:";
      this.prt_Margin_TopLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      //
      //prt_CancelButton
      //
      this.prt_CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.prt_CancelButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_CancelButton.Location = new System.Drawing.Point(472, 364);
      this.prt_CancelButton.Name = "prt_CancelButton";
      this.prt_CancelButton.Size = new System.Drawing.Size(80, 24);
      this.prt_CancelButton.TabIndex = 2;
      this.prt_CancelButton.Text = "Cancel";
      //
      //prt_OKButton
      //
      this.prt_OKButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.prt_OKButton.Location = new System.Drawing.Point(386, 364);
      this.prt_OKButton.Name = "prt_OKButton";
      this.prt_OKButton.Size = new System.Drawing.Size(80, 24);
      this.prt_OKButton.TabIndex = 1;
      this.prt_OKButton.Text = "OK";
      //
      //PrintOptionsDlg
      //
      this.AcceptButton = this.prt_OKButton;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this.prt_CancelButton;
      this.ClientSize = new System.Drawing.Size(569, 408);
      this.Controls.Add(this.prt_OKButton);
      this.Controls.Add(this.prt_CancelButton);
      this.Controls.Add(this.TabControl1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "PrintOptionsDlg";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Sheet Print Options";
      this.TabControl1.ResumeLayout(false);
      this.prt_TabGeneral.ResumeLayout(false);
      this.prt_TabGeneral.PerformLayout();
      this.prt_TabSmartPrint.ResumeLayout(false);
      this.prt_TabSmartPrint.PerformLayout();
      this.prt_TabOutput.ResumeLayout(false);
      this.prt_TabHeadFoot.ResumeLayout(false);
      this.prt_TabHeadFoot.PerformLayout();
      this.Panel1.ResumeLayout(false);
      this.prt_TabPagination.ResumeLayout(false);
      this.prt_TabPagination.PerformLayout();
      this.prt_TabMargins.ResumeLayout(false);
      this.prt_TabMargins.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private void FormExchange(bool isInit)
    {
      // Set up data exchanges between the various controls and their associated properties in the spreadsheet
      // General Tab
      bool Preview = SpreadWrapper1.ActiveSheet.PrintInfo.Preview;
      ExchangeBoolProp(isInit, ref Preview, this.prt_Gen_Preview);
      SpreadWrapper1.ActiveSheet.PrintInfo.Preview = Preview;

      bool UseMax = SpreadWrapper1.ActiveSheet.PrintInfo.UseMax;
      ExchangeBoolProp(isInit, ref UseMax, this.prt_Gen_UseMax);
      SpreadWrapper1.ActiveSheet.PrintInfo.UseMax = UseMax;

      string Printer = SpreadWrapper1.ActiveSheet.PrintInfo.Printer;
      ExchangeStringProp(isInit, ref Printer, this.prt_Gen_Printer, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.Printer = Printer;

      string JobName = SpreadWrapper1.ActiveSheet.PrintInfo.JobName;
      ExchangeStringProp(isInit, ref JobName, this.prt_Gen_JobName, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.JobName = JobName;

      string AbortMessage = SpreadWrapper1.ActiveSheet.PrintInfo.AbortMessage;
      ExchangeStringProp(isInit, ref AbortMessage, this.prt_Gen_AbortMessage, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.AbortMessage = AbortMessage;

      bool BestFitCols = SpreadWrapper1.ActiveSheet.PrintInfo.BestFitCols;
      ExchangeBoolProp(isInit, ref BestFitCols, this.prt_Gen_BestFitCols);
      SpreadWrapper1.ActiveSheet.PrintInfo.BestFitCols = BestFitCols;

      bool BestFitRows = SpreadWrapper1.ActiveSheet.PrintInfo.BestFitRows;
      ExchangeBoolProp(isInit, ref BestFitRows, this.prt_Gen_BestFitRows);
      SpreadWrapper1.ActiveSheet.PrintInfo.BestFitRows = BestFitRows;

      // Convert the ZoomFactor to percentage for UI ease of use (value = 1, then percentage = 100)
      if ((isInit == true))
      {
        this.zoomFactor = SpreadWrapper1.ActiveSheet.PrintInfo.ZoomFactor * 100;
        string zoom = zoomFactor.ToString();
        ExchangeStringProp(isInit, ref zoom, this.prt_Gen_ZoomFactor, false);
        zoomFactor = float.Parse(zoom);
      }
      else
      {
        string zoom = zoomFactor.ToString();
        ExchangeStringProp(isInit, ref zoom, this.prt_Gen_ZoomFactor, false);
        zoomFactor = float.Parse(zoom);

        if ((this.zoomFactor < 10f & this.zoomFactor != 0))
          this.zoomFactor = 10f;
        // 16603

        if ((this.zoomFactor > 400f))
          this.zoomFactor = 400f;
        // 16603

        SpreadWrapper1.ActiveSheet.PrintInfo.ZoomFactor = this.zoomFactor / 100;
      }

      // Output Tab
      bool ShowBorder = SpreadWrapper1.ActiveSheet.PrintInfo.ShowBorder;
      ExchangeBoolProp(isInit, ref ShowBorder, this.prt_Out_ShowBorder);
      SpreadWrapper1.ActiveSheet.PrintInfo.ShowBorder = ShowBorder;

      bool ShowColor = SpreadWrapper1.ActiveSheet.PrintInfo.ShowColor;
      ExchangeBoolProp(isInit, ref ShowColor, this.prt_Out_ShowColor);
      SpreadWrapper1.ActiveSheet.PrintInfo.ShowColor = ShowColor;

      //ShowColumnHeaders changed from Inherit to True unexpectedly due to using ByRef
      if ((isInit))
      {
        ExchangeBoolPropIn(SpreadWrapper1.ActiveSheet.PrintInfo.ShowColumnHeaders, this.prt_Out_ShowColumnHeaders);
        ExchangeBoolPropIn(SpreadWrapper1.ActiveSheet.PrintInfo.ShowRowHeaders, this.prt_Out_ShowRowHeaders);
      }
      else
      {
        if ((SpreadWrapper1.ActiveSheet.PrintInfo.ShowColumnHeaders != this.prt_Out_ShowColumnHeaders.Checked))
        {
          bool ShowColumnHeaders = SpreadWrapper1.ActiveSheet.PrintInfo.ShowColumnHeaders;
          ExchangeBoolPropOut(ref ShowColumnHeaders, this.prt_Out_ShowColumnHeaders);
          SpreadWrapper1.ActiveSheet.PrintInfo.ShowColumnHeaders = ShowColumnHeaders;

        }
        if ((SpreadWrapper1.ActiveSheet.PrintInfo.ShowRowHeaders != this.prt_Out_ShowRowHeaders.Checked))
        {
          bool ShowRowHeaders = SpreadWrapper1.ActiveSheet.PrintInfo.ShowRowHeaders;
          ExchangeBoolPropOut(ref ShowRowHeaders, this.prt_Out_ShowRowHeaders);
          SpreadWrapper1.ActiveSheet.PrintInfo.ShowRowHeaders = ShowRowHeaders;
        }
      }
      bool ShowGrid = SpreadWrapper1.ActiveSheet.PrintInfo.ShowGrid;
      bool ShowPrintDialog = SpreadWrapper1.ActiveSheet.PrintInfo.ShowPrintDialog;
      bool ShowShadows = SpreadWrapper1.ActiveSheet.PrintInfo.ShowShadows;

      ExchangeBoolProp(isInit, ref ShowGrid, this.prt_Out_ShowGrid);
      SpreadWrapper1.ActiveSheet.PrintInfo.ShowGrid = ShowGrid;

      ExchangeBoolProp(isInit, ref ShowPrintDialog, this.prt_Out_ShowPrintDialog);
      SpreadWrapper1.ActiveSheet.PrintInfo.ShowPrintDialog = ShowPrintDialog;

      ExchangeBoolProp(isInit, ref ShowShadows, this.prt_Out_ShowShadows);
      SpreadWrapper1.ActiveSheet.PrintInfo.ShowShadows = ShowShadows;

      int PrintType = (int)SpreadWrapper1.ActiveSheet.PrintInfo.PrintType;
      int Orientation = (int)SpreadWrapper1.ActiveSheet.PrintInfo.Orientation;

      ExchangeEnumComboProp(isInit, ref PrintType, this.prt_Out_PrintType);
      SpreadWrapper1.ActiveSheet.PrintInfo.PrintType = (PrintType)PrintType;

      ExchangeEnumComboProp(isInit, ref Orientation, this.prt_Out_Orientation);
      SpreadWrapper1.ActiveSheet.PrintInfo.Orientation = (PrintOrientation)Orientation;
      // Header/Footer Tab
      if ((isInit == true))
      {
        this.prt_HeadFoot_Header.Checked = true;
        this.prt_HeadFoot_ControlChars.SelectedIndex = 0;
      }

      string Header = SpreadWrapper1.ActiveSheet.PrintInfo.Header;
      ExchangeStringProp(isInit, ref Header, this.prt_HeadFoot_HeaderText, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.Header = Header;

      string Footer = SpreadWrapper1.ActiveSheet.PrintInfo.Footer;
      ExchangeStringProp(isInit, ref Footer, this.prt_HeadFoot_FooterText, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.Footer = Footer;

      // SmartPrint Tab
      bool UseSmartPrint = SpreadWrapper1.ActiveSheet.PrintInfo.UseSmartPrint;
      ExchangeBoolProp(isInit, ref UseSmartPrint, this.prt_SmartPrint_UseSmartPrint);
      SpreadWrapper1.ActiveSheet.PrintInfo.UseSmartPrint = UseSmartPrint;
      if ((isInit == true))
      {
        this.prt_SmartPrint_Rule.SelectedIndex = 0;
        this.prt_SmartPrint_ResetOption.SelectedIndex = 0;
        LoadRulesList(SpreadWrapper1.ActiveSheet.PrintInfo.SmartPrintRules);
      }
      else
      {
        SpreadWrapper1.ActiveSheet.PrintInfo.SmartPrintRules = this.rulesCollection;
      }

      // Pagination Tab
      string FirstPageNumber = SpreadWrapper1.ActiveSheet.PrintInfo.FirstPageNumber.ToString();
      ExchangeStringProp(isInit, ref FirstPageNumber, this.prt_Pag_FirstPageNumber, false);
      SpreadWrapper1.ActiveSheet.PrintInfo.FirstPageNumber = int.Parse(FirstPageNumber);

      int PageOrder = (int)SpreadWrapper1.ActiveSheet.PrintInfo.PageOrder;
      ExchangeEnumComboProp(isInit, ref PageOrder, this.prt_Pag_PageOrder);
      SpreadWrapper1.ActiveSheet.PrintInfo.PageOrder = (PrintPageOrder)(PageOrder);

      string PageStart = SpreadWrapper1.ActiveSheet.PrintInfo.PageStart.ToString();
      ExchangeStringProp(isInit, ref PageStart, this.prt_Pag_PageStart, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.PageStart = int.Parse(PageStart);

      string PageEnd = SpreadWrapper1.ActiveSheet.PrintInfo.PageEnd.ToString();
      ExchangeStringProp(isInit, ref PageEnd, this.prt_Pag_PageEnd, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.PageEnd = int.Parse(PageEnd);

      string ColStart = SpreadWrapper1.ActiveSheet.PrintInfo.ColStart.ToString();
      ExchangeStringProp(isInit, ref ColStart, this.prt_Pag_ColumnStart, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.ColStart = int.Parse(ColStart);

      string ColEnd = SpreadWrapper1.ActiveSheet.PrintInfo.ColEnd.ToString();
      ExchangeStringProp(isInit, ref ColEnd, this.prt_Pag_ColumnEnd, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.ColEnd = int.Parse(ColEnd);

      string RowStart = SpreadWrapper1.ActiveSheet.PrintInfo.RowStart.ToString();
      ExchangeStringProp(isInit, ref RowStart, this.prt_Pag_RowStart, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.RowStart = int.Parse(RowStart);

      string RowEnd = SpreadWrapper1.ActiveSheet.PrintInfo.RowEnd.ToString();
      ExchangeStringProp(isInit, ref RowEnd, this.prt_Pag_RowEnd, true);
      SpreadWrapper1.ActiveSheet.PrintInfo.RowEnd = int.Parse(RowEnd);

      // Margins Tab
      if ((isInit == true))
      {
        this.marginsTop = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Top / 100;
        this.marginsLeft = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Left / 100;
        this.marginsRight = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Right / 100;
        this.marginsBottom = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Bottom / 100;
        this.marginsHeader = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Header / 100;
        this.marginsFooter = SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Footer / 100;

        string mgTop = marginsTop.ToString();
        ExchangeStringProp(isInit, ref mgTop, this.prt_Margin_Top, false);
        marginsTop = int.Parse(mgTop);

        string mgLeft = marginsLeft.ToString();
        ExchangeStringProp(isInit, ref mgLeft, this.prt_Margin_Left, false);
        marginsLeft = int.Parse(mgLeft);

        string mgRight = marginsRight.ToString();
        ExchangeStringProp(isInit, ref mgRight, this.prt_Margin_Right, false);
        marginsRight = int.Parse(mgRight);

        string mgBottom = marginsBottom.ToString();
        ExchangeStringProp(isInit, ref mgBottom, this.prt_Margin_Bottom, false);
        marginsBottom = int.Parse(mgBottom);


        string mgHeader = marginsHeader.ToString();
        ExchangeStringProp(isInit, ref mgHeader, this.prt_Margin_Header, false);
        marginsHeader = int.Parse(mgHeader);

        string mgFooter = marginsFooter.ToString();
        ExchangeStringProp(isInit, ref mgFooter, this.prt_Margin_Footer, false);
        marginsFooter = int.Parse(mgFooter);
      }
      else
      {
        string mgTop = marginsTop.ToString();
        ExchangeStringProp(isInit, ref mgTop, this.prt_Margin_Top, false);
        marginsTop = int.Parse(mgTop);


        string mgLeft = marginsLeft.ToString();
        ExchangeStringProp(isInit, ref mgLeft, this.prt_Margin_Left, false);
        marginsLeft = int.Parse(mgLeft);


        string mgRight = marginsRight.ToString();
        ExchangeStringProp(isInit, ref mgRight, this.prt_Margin_Right, false);
        marginsRight = int.Parse(mgRight);

        string mgBottom = marginsBottom.ToString();
        ExchangeStringProp(isInit, ref mgBottom, this.prt_Margin_Bottom, false);
        marginsBottom = int.Parse(mgBottom);


        string mgHeader = marginsHeader.ToString();
        ExchangeStringProp(isInit, ref mgHeader, this.prt_Margin_Header, false);
        marginsHeader = int.Parse(mgHeader);

        string mgFooter = marginsFooter.ToString();
        ExchangeStringProp(isInit, ref mgFooter, this.prt_Margin_Footer, false);
        marginsFooter = int.Parse(mgFooter);

        
        
        

        if ((this.marginsTop < 0))
          this.marginsTop = 0;
        if ((this.marginsLeft < 0))
          this.marginsLeft = 0;
        if ((this.marginsRight < 0))
          this.marginsRight = 0;
        if ((this.marginsBottom < 0))
          this.marginsBottom = 0;
        if ((this.marginsHeader < 0))
          this.marginsHeader = 0;
        if ((this.marginsFooter < 0))
          this.marginsFooter = 0;

        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Top = (int)this.marginsTop * 100;
        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Left = (int)this.marginsLeft * 100;
        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Right = (int)this.marginsRight * 100;
        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Bottom = (int)this.marginsBottom * 100;
        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Header = (int)this.marginsHeader * 100;
        SpreadWrapper1.ActiveSheet.PrintInfo.Margin.Footer = (int)this.marginsFooter * 100;
      }

    }

    // Function to handle loading the SmartPrint Rules into an easily readable format
    private void LoadRulesList(SmartPrintRulesCollection rules)
	{
		string ruleString = "";
		// Clear rules listbox
		this.prt_SmartPrint_RulesCollection.Items.Clear();

		// Loop through rules collection and convert to readable format from values
    foreach ( FarPoint.Win.Spread.SmartPrintRule rule in SpreadWrapper1.ActiveSheet.PrintInfo.SmartPrintRules)
    {
			// Add a rule to a collection that mirrors the human readable list of rules
			this.rulesCollection.Add(rule);

			//Bug99914384 Preston(4-Mar-2010) 
			if (((rule) is ScaleRule)) {
				ruleString = rm.GetString("prt_SmartPrintRules1") + ", Reset Option = " + rule.ResetOption.ToString();
			} else if (((rule) is LandscapeRule)) {
				ruleString = rm.GetString("prt_SmartPrintRules2") + ", Reset Option = " + rule.ResetOption.ToString();
			} else if (((rule) is BestFitColumnRule)) {
				ruleString = rm.GetString("prt_SmartPrintRules3") + ", Reset Option = " + rule.ResetOption.ToString();


			} else if (((rule) is SmartPaperRule)) {
				ruleString = rm.GetString("prt_SmartPrintRules4") + ", Reset Option = " + rule.ResetOption.ToString();

			}

			//scale rule
			if (((rule) is ScaleRule)) {
				ruleString = ruleString + ", Start Factor = " + ((ScaleRule)rule).StartFactor.ToString() + ", End Factor = " + ((ScaleRule)rule).EndFactor.ToString() + ", Interval = " + ((ScaleRule)rule).Interval.ToString();
			}
			// Once readable string is built, add to listbox
			this.prt_SmartPrint_RulesCollection.Items.Add(ruleString);

		}

		if ((this.prt_SmartPrint_RulesCollection.Items.Count == 0)) {
			this.prt_SmartPrint_DeleteButton.Enabled = false;
		} else {
			this.prt_SmartPrint_DeleteButton.Enabled = true;
			this.prt_SmartPrint_RulesCollection.SelectedIndex = 0;
		}

	}

    // Function to override paint of labels to add a separator bar
    private void Label_Paint(System.Object sender, System.Windows.Forms.PaintEventArgs e)
    {
      // Call common function to paint the separator bar
      common.PaintSeparator(sender, e);
    }

    // Function to exchange string data between controls and the string properties of the spreadsheet 
    private void ExchangeStringProp(bool isInit, ref string val, System.Windows.Forms.TextBox textbox, bool negHide)
    {
      // Make sure we have a valid control
      if ((textbox == null == false))
      {
        // If we are setting the text of the control
        if (isInit == true)
        {
          // If certain values are -1, we may want to represent them as empty for default values
          if ((negHide == true && (val == null == false)))
          {
            if ((val.Equals("-1")))
            {
              textbox.Text = "";
            }
            else
            {
              textbox.Text = val;
            }
          }
          else
          {
            textbox.Text = val;
          }
        }
        else
        {
          // If certain values are -1, we may want to get them as they are, not as an empty string 
          if ((negHide == true && textbox.Text.Length == 0))
          {
            val = "-1";
          }
          else
          {
            // Get the string for the property
            val = textbox.Text;
          }
        }
      }
    }

    // Function to exchange true/false data between controls and the boolean properties of the spreadsheet 
    private void ExchangeBoolProp(bool isInit, ref bool val, System.Windows.Forms.CheckBox checkBox)
    {
      // Make sure we have a valid control
      if ((checkBox != null))
      {
        if (isInit == true)
        {
          // Set the checked state of the control to represent the true/false state of the property
          checkBox.Checked = val;
        }
        else
        {
          // Get the property as the checked state of the control
          //99913883 Justin
          if (val != checkBox.Checked)
          {
            val = checkBox.Checked;
          }
        }
      }
    }


    private void ExchangeBoolPropIn(bool val, System.Windows.Forms.CheckBox checkBox)
    {
      // Make sure we have a valid control
      if ((checkBox == null == false))
      {
        // Set the checked state of the control to represent the true/false state of the property
        checkBox.Checked = val;
      }
    }

    private void ExchangeBoolPropOut(ref bool val, System.Windows.Forms.CheckBox checkBox)
    {
      // Make sure we have a valid control
      if ((checkBox == null == false))
      {
        // Get the property as the checked state of the control
        if (val != checkBox.Checked)
        {
          val = checkBox.Checked;
        }
      }
    }


    // Function to exchange enumerated data between comboboxes and the enumerated properties of the spreadsheet 
    private void ExchangeEnumComboProp(bool isInit, ref int val, System.Windows.Forms.ComboBox comboBox)
    {
      // Make sure we have a valid control
      if ((comboBox == null == false))
      {
        if (isInit == true)
        {
          // Set the index of the combobox to represent the enumerated value of the property
          comboBox.SelectedIndex = val;
        }
        else
        {
          // Get the index of the combobox as the enumerated value of the property
          val = comboBox.SelectedIndex;
        }
      }
    }

    // Structure representing a button object and its associated integer value
    private struct EnumButtonList
    {
      public object button;
      public int buttonVal;
    }

    
    private void PrintOptionsDlg_Load(System.Object sender, System.EventArgs e)
    {
      if ((rm != null))
      {
        this.Text = rm.GetString("PrintOptionsDlg");

        prt_TabGeneral.Text = rm.GetString("prt_TabGeneral");
        prt_Gen_ZoomPctLabel.Text = rm.GetString("prt_Gen_ZoomPctLabel");
        prt_Gen_BestFitRows.Text = rm.GetString("prt_Gen_BestFitRows");
        prt_Gen_BestFitCols.Text = rm.GetString("prt_Gen_BestFitCols");
        prt_Gen_ZoomLabel.Text = rm.GetString("prt_Gen_ZoomLabel");
        prt_Gen_UseMax.Text = rm.GetString("prt_Gen_UseMax");
        prt_Gen_AbortMsgLabel.Text = rm.GetString("prt_Gen_AbortMsgLabel");
        prt_Gen_PrintJobLabel.Text = rm.GetString("prt_Gen_PrintJobLabel");
        prt_Gen_PrinterNameLabel.Text = rm.GetString("prt_Gen_PrinterNameLabel");
        prt_Gen_Preview.Text = rm.GetString("prt_Gen_Preview");
        prt_TabOutput.Text = rm.GetString("prt_TabOutput");
        prt_Out_OrientationLabel.Text = rm.GetString("prt_Out_OrientationLabel");
        prt_Out_TypeLabel.Text = rm.GetString("prt_Out_TypeLabel");
        prt_Out_ShowPrintDialog.Text = rm.GetString("prt_Out_ShowPrintDialog");
        prt_Out_ShowShadows.Text = rm.GetString("prt_Out_ShowShadows");
        prt_Out_ShowGrid.Text = rm.GetString("prt_Out_ShowGrid");
        prt_Out_ShowRowHeaders.Text = rm.GetString("prt_Out_ShowRowHeaders");
        prt_Out_ShowColumnHeaders.Text = rm.GetString("prt_Out_ShowColumnHeaders");
        prt_Out_ShowColor.Text = rm.GetString("prt_Out_ShowColor");
        prt_Out_ShowBorder.Text = rm.GetString("prt_Out_ShowBorder");
        prt_TabHeadFoot.Text = rm.GetString("prt_TabHeadFoot");
        prt_HeadFoot_Footer.Text = rm.GetString("prt_HeadFoot_Footer");
        prt_HeadFoot_Header.Text = rm.GetString("prt_HeadFoot_Header");
        prt_HeadFoot_AddButton.Text = rm.GetString("prt_HeadFoot_AddButton");
        prt_HeadFoot_ControlCharsLabel.Text = rm.GetString("prt_HeadFoot_ControlCharsLabel");
        prt_TabSmartPrint.Text = rm.GetString("prt_TabSmartPrint");
        prt_SmartPrint_DeleteButton.Text = rm.GetString("prt_SmartPrint_DeleteButton");
        prt_SmartPrint_IntervalLabel.Text = rm.GetString("prt_SmartPrint_IntervalLabel");
        prt_SmartPrint_EndLabel.Text = rm.GetString("prt_SmartPrint_EndLabel");
        prt_SmartPrint_StartLabel.Text = rm.GetString("prt_SmartPrint_StartLabel");
        prt_SmartPrint_ResetLabel.Text = rm.GetString("prt_SmartPrint_ResetLabel");
        prt_SmartPrint_OptionsLabel.Text = rm.GetString("prt_SmartPrint_OptionsLabel");
        prt_SmartPrint_AddButton.Text = rm.GetString("prt_SmartPrint_AddButton");
        prt_SmartPrint_RuleLabel.Text = rm.GetString("prt_SmartPrint_RuleLabel");
        prt_SmartPrint_UseSmartPrint.Text = rm.GetString("prt_SmartPrint_UseSmartPrint");
        prt_TabPagination.Text = rm.GetString("prt_TabPagination");
        prt_Pag_RowEndLabel.Text = rm.GetString("prt_Pag_RowEndLabel");
        prt_Pag_RowStartLabel.Text = rm.GetString("prt_Pag_RowStartLabel");
        prt_Pag_ColEndLabel.Text = rm.GetString("prt_Pag_ColEndLabel");
        prt_Pag_ColStartLabel.Text = rm.GetString("prt_Pag_ColStartLabel");
        prt_Pag_PageEndLabel.Text = rm.GetString("prt_Pag_PageEndLabel");
        prt_Pag_PageStartLabel.Text = rm.GetString("prt_Pag_PageStartLabel");
        prt_Pag_PageOrderLabel.Text = rm.GetString("prt_Pag_PageOrderLabel");
        prt_Pag_FirstPageLabel.Text = rm.GetString("prt_Pag_FirstPageLabel");
        prt_Pag_RowLabel.Text = rm.GetString("prt_Pag_RowLabel");
        prt_Pag_ColumnLabel.Text = rm.GetString("prt_Pag_ColumnLabel");
        prt_Pag_PageLabel.Text = rm.GetString("prt_Pag_PageLabel");
        prt_Pag_SettingsLabel.Text = rm.GetString("prt_Pag_SettingsLabel");
        prt_TabMargins.Text = rm.GetString("prt_TabMargins");
        prt_Margin_SizingNote.Text = rm.GetString("prt_Margin_SizingNote");
        prt_Margin_RegionsLabel.Text = rm.GetString("prt_Margin_RegionsLabel");
        prt_Margin_FooterLabel.Text = rm.GetString("prt_Margin_FooterLabel");
        prt_Margin_HeaderLabel.Text = rm.GetString("prt_Margin_HeaderLabel");
        prt_Margin_BottomLabel.Text = rm.GetString("prt_Margin_BottomLabel");
        prt_Margin_RightLabel.Text = rm.GetString("prt_Margin_RightLabel");
        prt_Margin_LeftLabel.Text = rm.GetString("prt_Margin_LeftLabel");
        prt_Margin_TopLabel.Text = rm.GetString("prt_Margin_TopLabel");
        prt_CancelButton.Text = rm.GetString("prt_CancelButton");
        prt_OKButton.Text = rm.GetString("prt_OKButton");


        prt_Out_Orientation.Items.Clear();
        prt_Out_Orientation.Items.AddRange(new object[] {
				rm.GetString("prt_Orientation1"),
				rm.GetString("prt_Orientation2"),
				rm.GetString("prt_Orientation3")
			});
        prt_Out_PrintType.Items.Clear();
        prt_Out_PrintType.Items.AddRange(new object[] {
				rm.GetString("prt_PrintType1"),
				rm.GetString("prt_PrintType2"),
				rm.GetString("prt_PrintType3"),
				rm.GetString("prt_PrintType4")
			});
        prt_HeadFoot_ControlChars.Items.Clear();
        prt_HeadFoot_ControlChars.Items.AddRange(new object[] {
				rm.GetString("prt_ControlChars1"),
				rm.GetString("prt_ControlChars2"),
				rm.GetString("prt_ControlChars3"),
				rm.GetString("prt_ControlChars4"),
				rm.GetString("prt_ControlChars5"),
				rm.GetString("prt_ControlChars6"),
				rm.GetString("prt_ControlChars7"),
				rm.GetString("prt_ControlChars8"),
				rm.GetString("prt_ControlChars9"),
				rm.GetString("prt_ControlChars10"),
				rm.GetString("prt_ControlChars11"),
				rm.GetString("prt_ControlChars12"),
				rm.GetString("prt_ControlChars13"),
				rm.GetString("prt_ControlChars14"),
				rm.GetString("prt_ControlChars15"),
				rm.GetString("prt_ControlChars16"),
				rm.GetString("prt_ControlChars17")
			});


        prt_HeadFoot_ControlChars.Items.AddRange(new object[] {
				rm.GetString("prt_ControlChars18"),
				rm.GetString("prt_ControlChars19"),
				rm.GetString("prt_ControlChars20"),
				rm.GetString("prt_ControlChars21")
			});


        prt_SmartPrint_ResetOption.Items.Clear();

        prt_SmartPrint_ResetOption.Items.AddRange(new object[] {
				rm.GetString("prt_ResetOptions1"),
				rm.GetString("prt_ResetOptions2"),
				rm.GetString("prt_ResetOptions3")
			});
        prt_SmartPrint_Rule.Items.Clear();


        prt_SmartPrint_Rule.Items.AddRange(new object[] {
				rm.GetString("prt_SmartPrintRules1"),
				rm.GetString("prt_SmartPrintRules2"),
				rm.GetString("prt_SmartPrintRules3"),
				rm.GetString("prt_SmartPrintRules4")
			});


        prt_Pag_PageOrder.Items.Clear();
        prt_Pag_PageOrder.Items.AddRange(new object[] {
				rm.GetString("prt_PageOrder1"),
				rm.GetString("prt_PageOrder2"),
				rm.GetString("prt_PageOrder3")
			});

      }

      // Set up the initial values in the controls on the form
      FormExchange(true);

      if ((this.Owner != null))
        this.Icon = this.Owner.Icon;
      // 21877

    }

    // Function to catch the click event of the OK button
    private void OK_Click(System.Object sender, System.EventArgs e)
    {
      float tmpZoomFactor = 0;
      string tmpZoom = tmpZoomFactor.ToString();
      ExchangeStringProp(false, ref tmpZoom, this.prt_Gen_ZoomFactor, false);
      tmpZoomFactor = int.Parse(tmpZoom);

      if ((((tmpZoomFactor < 10f) | (tmpZoomFactor > 400f)) && tmpZoomFactor != 0.0))
      {
        MessageBox.Show(rm.GetString("ZoomFactorWarning"), rm.GetString("InvalidValueTitle"));
        this.prt_Gen_ZoomFactor.Text = "100";
        // 16603
        this.prt_Gen_ZoomFactor.Focus();
      }
      else
      {
        // Retrieve the values of the controls on the form and set the properties of the spreadsheet
        FormExchange(false);
        // Set the return result
        this.DialogResult = System.Windows.Forms.DialogResult.OK;
      }
    }

    // Function to catch the click event of the Cancel button
    private void Cancel_Click(System.Object sender, System.EventArgs e)
    {
      // Set the return result
      this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
    }

    // Fucntion to handle when special characters are added to header/footer
    private void HeadFoot_Add_Clicked(System.Object sender, System.EventArgs e)
    {
      ArrayList controlChars = new ArrayList();
      // Set up an array of control characters and load their representations accordingly
      controlChars.AddRange(new object[] {
			"/n",
			"/l",
			"/r",
			"/c",
			"/p",
			"/pc",
			"//",
			"/fn",
			"/fz",
			"/fb0",
			"/fb1",
			"/fi0",
			"/fi1",
			"/fu0",
			"/fu1",
			"/fk0",
			"/fk1",
			"/dl",
			"/ds",
			"/tl",
			"/ts"
		});

      // If we are adding to the header
      if ((this.prt_HeadFoot_Header.Checked == true))
      {
        // Add new special characters to the header text
        this.prt_HeadFoot_HeaderText.Text = this.prt_HeadFoot_HeaderText.Text + controlChars[this.prt_HeadFoot_ControlChars.SelectedIndex];
        // adding to footer
      }
      else
      {
        // Add new special characters to the footer text
        this.prt_HeadFoot_FooterText.Text = this.prt_HeadFoot_FooterText.Text + controlChars[this.prt_HeadFoot_ControlChars.SelectedIndex];
      }
    }

    // Function to toggle the header/footer input box visibility
    private void HeadFoot_Header_CheckedChanged(System.Object sender, System.EventArgs e)
    {
      this.prt_HeadFoot_HeaderText.Visible = true;
      this.prt_HeadFoot_FooterText.Visible = false;
    }

    // Function to toggle the header/footer input box visibility
    private void HeadFoot_Footer_CheckedChanged(System.Object sender, System.EventArgs e)
    {
      this.prt_HeadFoot_FooterText.Visible = true;
      this.prt_HeadFoot_HeaderText.Visible = false;
    }

    // Function to handle when a Smart Print rule is changed in the listbox
    private void SmartPrint_Rule_SelectedIndexChanged(System.Object sender, System.EventArgs e)
    {
      //scale rule
      if ((this.prt_SmartPrint_Rule.SelectedIndex == 0))
      {
        // Show all controls that are specific to the scale rule and set values/text appropriately
        this.prt_SmartPrint_StartLabel.Visible = true;
        this.prt_SmartPrint_EndLabel.Visible = true;
        this.prt_SmartPrint_IntervalLabel.Visible = true;
        this.prt_SmartPrint_ScaleStartFactor.Visible = true;
        if ((string.IsNullOrEmpty(this.prt_SmartPrint_ScaleStartFactor.Text)))
        {
          this.prt_SmartPrint_ScaleStartFactor.Text = ((int)1).ToString("0.0");
          // 20828
        }
        this.prt_SmartPrint_ScaleEndFactor.Visible = true;
        if ((string.IsNullOrEmpty(this.prt_SmartPrint_ScaleEndFactor.Text)))
        {
          this.prt_SmartPrint_ScaleEndFactor.Text = ((int)1).ToString("0.0");
          // 20828
        }
        this.prt_SmartPrint_ScaleInterval.Visible = true;
        if ((string.IsNullOrEmpty(this.prt_SmartPrint_ScaleInterval.Text)))
        {
          this.prt_SmartPrint_ScaleInterval.Text = ((int)1).ToString("0.0");
          // 20828
        }
      }
      else
      {
        // Hide all controls that are specific to the scale rule and set values/text appropriately
        this.prt_SmartPrint_StartLabel.Visible = false;
        this.prt_SmartPrint_EndLabel.Visible = false;
        this.prt_SmartPrint_IntervalLabel.Visible = false;
        this.prt_SmartPrint_ScaleStartFactor.Visible = false;
        this.prt_SmartPrint_ScaleEndFactor.Visible = false;
        this.prt_SmartPrint_ScaleInterval.Visible = false;
      }
    }

    // Function to handle when a new Smart Print rule is added
    private void SmartPrint_Add_Click(System.Object sender, System.EventArgs e)
    {
      string ruleString = null;

      // Retrieve the custom rule as a string
      ruleString = this.prt_SmartPrint_Rule.SelectedItem.ToString();
      ruleString = ruleString + ", Reset Option = " + this.prt_SmartPrint_ResetOption.SelectedItem.ToString();

      //scale rule
      if ((this.prt_SmartPrint_Rule.SelectedIndex == 0))
      {
        // Convert all information associated with a scale rule to the inherent vales
        float sf = 0;
        float ef = 0;
        float intvl = 0;
        try
        {
          sf = float.Parse(this.prt_SmartPrint_ScaleStartFactor.Text);
        }
        catch (InvalidCastException ex)
        {
          sf = 1.0f;
        }

        try
        {
          ef = float.Parse(this.prt_SmartPrint_ScaleEndFactor.Text);
        }
        catch (InvalidCastException ex)
        {
          ef = 1.0f;
        }

        try
        {
          intvl = float.Parse(this.prt_SmartPrint_ScaleInterval.Text);
        }
        catch (InvalidCastException ex)
        {
          intvl = 0.1f;
        }
        // Add the new rule to the underlying collection
        this.rulesCollection.Add(new ScaleRule((ResetOption)this.prt_SmartPrint_ResetOption.SelectedIndex, sf, ef, intvl));

        ruleString = ruleString + ", Start Factor = " + sf.ToString() + ", End Factor = " + ef.ToString() + ", Interval = " + intvl.ToString();

        //landscape rule
      }
      else if ((this.prt_SmartPrint_Rule.SelectedIndex == 1))
      {
        // Convert all information associated with a landscape rule to the inherent vales
        // Add the new rule to the underlying collection
        this.rulesCollection.Add(new LandscapeRule((ResetOption)this.prt_SmartPrint_ResetOption.SelectedIndex));
        //bestfit column rule
      }
      else if ((this.prt_SmartPrint_Rule.SelectedIndex == 2))
      {
        // Convert all information associated with a bestfit column rule to the inherent vales
        // Add the new rule to the underlying collection
        this.rulesCollection.Add(new BestFitColumnRule((ResetOption)this.prt_SmartPrint_ResetOption.SelectedIndex));

        //smart paper rule
      }
      else if ((this.prt_SmartPrint_Rule.SelectedIndex == 3))
      {
        // Convert all information associated with a 'smart paper rule to the inherent vales
        // Add the new rule to the underlying collection
        this.rulesCollection.Add(new SmartPaperRule((ResetOption)this.prt_SmartPrint_ResetOption.SelectedIndex));


      }

      // Add the string representation of the new rule to the listbox
      this.prt_SmartPrint_RulesCollection.Items.Add(ruleString);

      this.prt_SmartPrint_DeleteButton.Enabled = true;
      this.prt_SmartPrint_RulesCollection.SelectedIndex = this.prt_SmartPrint_RulesCollection.Items.Count - 1;

    }

    // Function to handle deleting a Smart Print rule
    private void SmartPrint_Delete_Click(System.Object sender, System.EventArgs e)
    {
      // Remove the rule from the underlying collection
      this.rulesCollection.RemoveAt(this.prt_SmartPrint_RulesCollection.SelectedIndex);
      // Remove the rule from the listbox
      this.prt_SmartPrint_RulesCollection.Items.RemoveAt(this.prt_SmartPrint_RulesCollection.SelectedIndex);
      if ((this.prt_SmartPrint_RulesCollection.Items.Count > 0))
      {
        this.prt_SmartPrint_RulesCollection.SelectedIndex = 0;
      }
      else
      {
        this.prt_SmartPrint_DeleteButton.Enabled = false;
      }
    }

    // Function to handle verification of numeric float values entered into a textbox
    private void Float_TextChanged(System.Object sender, System.ComponentModel.CancelEventArgs e)
    {
      // Verify text entered into textbox is actually a number that will be converted to a float value
      //>>Eric bug #99903332 20081120 
      bool valueOutOfRange = false;
      //<< bug #99903332 
      try
      {
        float x = 0;
        if ((!string.IsNullOrEmpty(((TextBox)sender).Text)))
        {
          x = Convert.ToSingle(((TextBox)sender).Text);
          //>>Eric bug #99902970 20081014 
          //Else '99902050 mark
          //  MessageBox.Show(rm.GetString("OnlyNumbersWarning"), rm.GetString("InvalidValueTitle"))
          //  e.Cancel = True
        }
        else if ((sender.Equals(prt_Margin_Bottom)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Margin_Footer)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Margin_Header)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Margin_Left)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Margin_Right)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Margin_Top)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_Gen_ZoomFactor)))
        {
          ((TextBox)sender).Text = "0";
        }
        else if ((sender.Equals(prt_SmartPrint_ScaleInterval)))
        {
          ((TextBox)sender).Text = "0.1";
        }
        else if ((sender.Equals(prt_SmartPrint_ScaleEndFactor) | sender.Equals(prt_SmartPrint_ScaleStartFactor)))
        {
          ((TextBox)sender).Text = "1.0";
        }
        //<< bug #99902970
        //>>Eric bug #99903332 20081120 
        if ((!string.IsNullOrEmpty(((TextBox)sender).Text)))
        {
          if (((sender.Equals(prt_SmartPrint_ScaleInterval)) & ((x <= 0) | (x >= 1))))
          {
            valueOutOfRange = true;
            throw new Exception(rm.GetString("ValueMustBeBetweenZeroAndOne"));
          }
          if (((sender.Equals(prt_SmartPrint_ScaleEndFactor) | sender.Equals(prt_SmartPrint_ScaleStartFactor)) & x <= 0))
          {
            valueOutOfRange = true;
            throw new Exception(rm.GetString("MustBePositiveValue"));
          }
        }
        //<< bug #99903332 
      }
      catch (Exception ex)
      {
        // Notify user of invalid input and reset the control for re-input
        //>>Eric bug #99903332 20081120 
        if (valueOutOfRange)
        {
          MessageBox.Show(ex.Message, rm.GetString("InvalidValueTitle"));
        }
        else
        {
          MessageBox.Show(rm.GetString("OnlyNumbersWarning"), rm.GetString("InvalidValueTitle"));
        }
        //<< bug #99903332 
        if ((sender.Equals(prt_Gen_ZoomFactor)))
        {
          ((TextBox)sender).Text = "100";
        }
        else if ((sender.Equals(prt_SmartPrint_ScaleInterval)))
        {
          ((TextBox)sender).Text = "0.1";
        }
        else if ((sender.Equals(prt_SmartPrint_ScaleEndFactor) | sender.Equals(prt_SmartPrint_ScaleStartFactor)))
        {
          ((TextBox)sender).Text = "1.0";
        }
        else
        {
          ((TextBox)sender).Text = "0";
        }
        ((TextBox)sender).SelectAll();
        ((TextBox)sender).Focus();
        e.Cancel = true;
        //99902050
      }

    }
    // Function to handle verification of numeric integer values entered into a textbox
    private void Integer_TextChanged(System.Object sender, System.ComponentModel.CancelEventArgs e)
    {
      // Verify text entered into textbox is actually a number that will be converted to an integer value
      try
      {
        int x = 0;
        if ((!string.IsNullOrEmpty(((TextBox)sender).Text)))
        {
          x = Convert.ToInt32(((TextBox)sender).Text);
          //Else '99902050 mark
          //  MessageBox.Show(rm.GetString("OnlyIntegersWarning"), rm.GetString("InvalidValueTitle"))
          //  e.Cancel = True
        }
        //>>Eric bug #99903503 20081128 : Must handle all kind of exception in case the number is to big that cause overflow
      }
      catch (Exception ex)
      {
        // Notify user of invalid input and reset the control for re-input
        MessageBox.Show(rm.GetString("OnlyIntegersWarning"), rm.GetString("InvalidValueTitle"));
        if ((sender.Equals(prt_Pag_FirstPageNumber)))
        {
          ((TextBox)sender).Text = "1";
        }
        else
        {
          ((TextBox)sender).Text = "";
        }
        ((TextBox)sender).SelectAll();
        ((TextBox)sender).Focus();
        e.Cancel = true;
        //99902050
      }
    }
    private void PrintOptionsDlg_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
    {
      if ((e.KeyCode == Keys.F1))
      {
        e.Handled = true;
        // Show help panel associated with the Print Options dialog        
      }
    }

  }
}
