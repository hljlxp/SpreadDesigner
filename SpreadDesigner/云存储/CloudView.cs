﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace SpreadDesigner
{
    public partial class CloudView : Form
    {
        SpreadDesigner m_mainFrm;
        public CloudView(SpreadDesigner mainFrm)
        {
            InitializeComponent();

            m_mainFrm = mainFrm;
        }

        private void Form1_Load(object sender123, EventArgs e123)
        {

        }

        private void button1List_Click(object sender, EventArgs e)
        {
            this.listView1.Items.Clear();
            QiniuWrapLib.mSingle.List(this.listView1);
        }

        private void button2Download_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                string key = this.listView1.SelectedItems[0].SubItems[0].Text;
                string fileName = QiniuWrapLib.mSingle.DownLoad(key);
                if (string.IsNullOrEmpty(fileName))
                {
                    MessageBox.Show("下载文件失败");
                    return;
                }

                MessageBox.Show("下载文件成功");
                //Spread Open
                m_mainFrm.ExcelOpenFile(fileName);
            }
        }

        private void button3Delete_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                string key = this.listView1.SelectedItems[0].SubItems[0].Text;

                if (MessageBox.Show("是否删除云端文件："+key, "提示",  MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                { 
                    bool ret = QiniuWrapLib.mSingle.Delete(key);
                    if (ret)
                    {
                        MessageBox.Show("云端删除文件成功");
                    }
                    else
                    {
                        MessageBox.Show("云端删除文件失败");
                    }
                }
            }
        }
    }
}
